let hackScript = 'early-hack-template.script';

class Server {
  constructor(ns, server) {
    this.ns = ns;
    this.server = server;
  }
  hack() {
    let me = this;
    let server = me.server;
    let myHackingLevel = me.ns.getHackingLevel();
    let hasRoot = me.ns.hasRootAccess(server);
    me.ns.tprint(`Server(${server}): has root ${hasRoot}`);
    if (hasRoot === false) {
      let requiredLevel = me.ns.getServerRequiredHackingLevel(server);
      if (requiredLevel <= myHackingLevel) {
        let portsOpened = 0;
        let portsRequired = me.ns.getServerNumPortsRequired(server);
        if (portsRequired > portsOpened && me.ns.fileExists("BruteSSH.exe", "home")) {
          me.ns.brutessh(target);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("FTPCrack.exe", "home")) {
          me.ns.ftpcrack(target);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("relaySMTP.exe", "home")) {
          me.ns.relaysmtp(target);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("HTTPWorm.exe", "home")) {
          me.ns.httpworm(target);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("SQLInject.exe", "home")) {
          me.ns.sqlinject(target);
          portsOpened++;
        }
        if (portsOpened >= portsRequired) {
          if (portsOpened > 0) {
            me.ns.tprint(`Server(${server}): Opened ${portsOpened}/${portsRequired} ports`);
          }
          me.ns.tprint(`Server(${server}): Rooting`);
          me.ns.nuke(server);
          me.ns.tprint(`Server(${server}): Rooted`);
          return true;
        } else {
          return false;
        }
      } else {
        me.ns.tprint(`Server(${server}): Required Level ${requiredLevel}`);
        return false;
      }
    } else {
      return true;
    }
  }
  hasScript(script) {
    return this.ns.fileExists(script, this.server);
  }
  putScript(script) {
    return this.ns.scp(script, this.server);
  }
  restartScript(script) {
    let me = this;
    if (me.ns.scriptRunning(script, me.server)) {
      me.ns.scriptKill(script, me.server);
    }
    me.ns.exec(script, me.server);
  }
  getScriptMaxThreads(script) {
    let me = this;
    let server = me.server;
    let scriptMem = me.ns.getScriptRam(script, server);
    // me.ns.tprint(`Script(${script}) Memory: ${scriptMem}`);
    let serverMaxRam = me.ns.getServerMaxRam(server);
    let serverUsedRam = me.ns.getServerUsedRam(server);
    let serverFreeRam = serverMaxRam - serverUsedRam;
    let scriptMaxThreads = Math.floor(serverFreeRam / scriptMem);
    return scriptMaxThreads;
    // me.ns.tprint(`Server(${server}): has ${serverFreeRam} free memory`);
    // me.ns.tprint(`Server(${server}): can run ${scriptMaxThreads} threads`);
  }
}

class Servers {
  constructor(ns) {
    this.ns = ns;
    this.loadServers();
  }
  loadServers() {
    let me = this;
    let servers = me.ns.scan();
    for (let s in servers) {
      let server = new Server(me.ns, servers[s]);
      let hacked = server.hack(server);
      server.getScriptMaxThreads(hackScript);
    }
  }
}

/** @param {NS} ns **/
export async function main(ns) {
  let servers = new Servers(ns);
}
