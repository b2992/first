import {NS} from "./index";

export async function main(ns: NS) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }

    await ns.wget(
        `https://gitlab.com/b2992/first/-/raw/main/loader.js?ts=${new Date().getTime()}`,
        "loader.js"
    );
    await ns.wget(
        `https://gitlab.com/b2992/first/-/raw/main/loaderFiles.js?ts=${new Date().getTime()}`,
        "loaderFiles.js"
    );
    ns.spawn("loader.js", 1);
}
