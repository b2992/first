import {NS} from "./index";
import {files} from "./loaderFiles.js";

const baseUrl = 'https://gitlab.com/b2992/first/-/raw/main/src'

export async function main(ns: NS) {
  ns.disableLog("sleep")

  for ( let filename of files ) {
    ns.scriptKill(filename, 'home')
    ns.rm(filename)
    await ns.sleep(50)
    let downloaded = await download(ns, filename)
    ns.tprint(`Downloaded ${filename}: ${downloaded}`)
  }
  await ns.sleep(50)
  ns.tprint('Killed and deleted old scripts.')
  await ns.sleep(50)
  ns.tprint(`Files downloaded.`)

  // Temporarily Disabled Loader Automatic Script Starting
  // await ns.sleep(50)
  // ns.tprint(`Starting /start.js`)
  // ns.spawn('/start.js', 1)
}

export async function download(ns: NS, filename: string) {
  const path = baseUrl+ '/' + filename
  ns.tprint(`Trying to download ${path} to ${filename}`)
  if (filename.includes('/')) {
    return await ns.wget(path, '/' + filename)
  } else {
    return await ns.wget(path, filename)
  }
}
