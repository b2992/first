import {NS} from "../../index";
import {factionServers as targets, fetchPlayer, findPath} from "common.js"

export function autocomplete(data: any, args: any[]) {
  return data.servers
}

export async function main(ns: NS) {
  let path;
  let player = fetchPlayer();

  if (ns.args[0] === undefined) {
    for (const server in targets) {
      ns.tprint("*********** " + server + " ( " + (targets as any)[server] + " faction)");
      path = await findPath(server);
      ns.tprint(printablePathToServer(path, true));
    }
  } else {
    path = await findPath(ns.args[0] as any);
    ns.tprint(printablePathToServer(path));
    if (player.sourceFiles.length > 0) {
      path.forEach((step) => ns.connect(step));
    }
  }
}

function printablePathToServer(path: string[], backdoor = false) {
  let msg = path.join("; connect ")
  if (backdoor) {
    msg += "; backdoor;"
  }
  return msg
}
