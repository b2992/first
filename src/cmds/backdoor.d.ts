import { NS } from "../../index";
export declare function main(ns: NS): Promise<void>;
export declare function autocomplete(data: any, args: any): any;
/**
 * @param {NS} ns
 * @param {string} target
 **/
export declare function backdoor(ns: NS, target: string): Promise<void>;
