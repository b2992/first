import { factionServers as targets, fetchPlayer, findPath } from "common.js";
export function autocomplete(data, args) {
    return data.servers;
}
export async function main(ns) {
    let path;
    let player = fetchPlayer();
    if (ns.args[0] === undefined) {
        for (const server in targets) {
            ns.tprint("*********** " + server + " ( " + targets[server] + " faction)");
            path = await findPath(server);
            ns.tprint(printablePathToServer(path, true));
        }
    }
    else {
        path = await findPath(ns.args[0]);
        ns.tprint(printablePathToServer(path));
        if (player.sourceFiles.length > 0) {
            path.forEach((step) => ns.connect(step));
        }
    }
}
function printablePathToServer(path, backdoor = false) {
    let msg = path.join("; connect ");
    if (backdoor) {
        msg += "; backdoor;";
    }
    return msg;
}
