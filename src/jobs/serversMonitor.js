import { networkMap, setLSItem, updateData } from "common.js";
export async function main(ns) {
    let nMap = await networkMap(ns);
    for (let server in nMap) {
        updateData(ns, nMap[server]);
    }
    setLSItem('NMAP', nMap);
}
