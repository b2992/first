import {NS} from "../../index";
import {clearLSItem, disableLogs, networkMapFree, root, toolsCount} from "common.js";


export async function main(ns: NS) {
  disableLogs(ns, ['sleep']);

  let count = toolsCount()
  let servers = Object.values(await networkMapFree())
    .filter((s: any) => !s.data.hasAdminRights &&
      s.portsRequired <= count )

  if ( servers.length == 0 )
    return

  ns.tprint(`Nuking ${servers.length} servers with ${count} or less ports required.`)
  for ( const server of servers ) {
    root(ns, server)
  }
  ns.tprint(`SUCCESS: Nuked ${servers.map((s: any) => s.name).join(", ")}`)
  clearLSItem('nmap')
}
