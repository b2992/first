import {NS} from "../../index";
import {myMoney, reserve} from "common.js";

export async function main(ns: NS) {
  let coresCost = ns.getUpgradeHomeCoresCost();
  let ramCost = ns.getUpgradeHomeRamCost();

  let nextUpgrade = 'ram';
  let nextCost = ramCost;
  if (coresCost < ramCost) {
    nextUpgrade = 'cores';
    nextCost = coresCost;
  }
  let upgraded = false;
  if ((myMoney(ns) - reserve(ns)) < nextCost) {
    if (nextUpgrade === 'cores') {
      upgraded = ns.upgradeHomeCores();
    }
    else {
      upgraded = ns.upgradeHomeRam();
    }
  }
  if (upgraded) {
    ns.tprint(`INFO Upgraded Home ${nextUpgrade} for ${ns.nFormat(nextCost, '$0,000a')}`);
  }
}
