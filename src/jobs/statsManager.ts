import {NS} from "../../index";
import {announce, fetchPlayer, findPath} from "common.js";

export async function main(ns: NS) {
  const doc = eval('document');
  const hook0 = doc.getElementById('overview-extra-hook-0');
  const hook1 = doc.getElementById('overview-extra-hook-1');
  if (!hook0 || !hook1) {
    return;
  }
  hook0.innerText = `Income\nExper.\n`;
  hook1.innerText =
    `${ns.nFormat((ns as any).getScriptIncome()[0], '$0,0a')}\n` +
    `${ns.nFormat((ns as any).getScriptExpGain(), '0,0a')}`;
}
