import {NS} from "../../index";
import {clearLSItem, disableLogs, fetchPlayer, networkMapFree, purchasables, root, toolsCount, tryRun} from "common.js";


export async function main(ns: NS) {
  disableLogs(ns, ['sleep']);
  const player = fetchPlayer();
  if (player.tor && player.boughtAllPrograms) {
    return;
  }
  if (!player.tor) {
    if (player.money > 2e5) {
      ns.purchaseTor();
      clearLSItem('nMap');
    }
    return;
  }

  for (const file of purchasables) {
    if (!player.programs.includes(file.name)) {
      if (player.money > file.cost) {
        ns.tprint(`INFO: Buying ${file.name} for ${ns.nFormat(file.cost, '$0,000a')}`);
        let purchased = ns.purchaseProgram(file.name);
        if (purchased) {
          ns.tprint(`INFO: Successfully purchased ${file.name}`);
        }
        // await tryRun(ns, () => { ns.run('/cmds/programBuyer.js', 1, file.name); });
      }
    }
  }
}
