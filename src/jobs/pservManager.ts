import {NS} from "../../index";
import {announce, fetchPlayer, formatRam, myMoney, networkMap, networkMapFree, reserve, tryRun} from "common.js";

const min = 60;
const hour = min * 50;

export async function main(ns: NS) {
  const homePS = ns.ps('home');
  if ( homePS.some(proc => proc.filename === 'buyer.js') ) {
    return
  }

  const nMap = await networkMapFree();
  const pServers: any[] = Object.values(nMap).filter((s: any) => { return s.name !== 'home' && s.data.purchasedByPlayer; });
  const curRam = smallestSize(pServers);
  const nextRam = nextRamSize(ns, curRam);

  if (nextRam === 0) {
    return;
  }

  let msg = `Running buyer.js to purchase ${formatRam(2**nextRam)} (currently ${formatRam(curRam)})`;
  announce(ns, msg);
  ns.tprint(msg);
  ns.tprint(`ns.spawn('buyer.js', 1, '--size', ${nextRam})`);
  ns.spawn('buyer.js', 1, '--size', String(nextRam));
}

function smallestSize(pServers: any[]) {
  if (pServers.length === 0) {
    return 0;
  }
  else {
    return pServers.reduce((prev, cur) => { return prev.maxRam < cur.maxRam ? prev : cur; }).maxRam;
  }
}

function nextRamSize(ns: NS, curRam: number): number {
  const limit = ns.getPurchasedServerLimit();
  const totIncomePerSec = (ns as any).getScriptIncome()[0];
  const maxServerSize = ns.getPurchasedServerMaxRam();
  const incomePerPayoffTime = totIncomePerSec * 2*hour;
  ns.print(`Total income: ${ns.nFormat(totIncomePerSec, "$0,0")}`)
  ns.print(`Income per payoff time: ${ns.nFormat(incomePerPayoffTime, "$0,0")}`)
  if (incomePerPayoffTime == 0) return 0

  let cost, totalCost
  for (let i = 20; 2**i > curRam; i--) {
    // max server size can vary based on BN
    if ( 2**i > maxServerSize ) continue;
    if (i < 0) { ns.tail(); throw `How is i less than 0? ${i}` }
    cost = ns.getPurchasedServerCost(2**i);
    totalCost = cost * limit;

    ns.print(`Total cost for ${2**i}GB ram: ${ns.nFormat(totalCost, "$0,0")}`);
    if ( totalCost < incomePerPayoffTime ) {
      ns.print(`(${2**i}) totalCost < incomePerPayoffTime`);
      ns.print(`Returning ${2**i}`);
      return i;
    }
  }
  return 0
}
