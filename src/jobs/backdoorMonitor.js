import { fetchPlayer, networkMap, tryRun } from "common.js";
export async function main(ns) {
    let nMap = await networkMap(ns);
    let player = await fetchPlayer();
    if (player.sourceFiles.length !== 0) {
        for (const server of Object.values(nMap)) {
            if (server.data.backdoorInstalled || server.data.purchasedByPlayer)
                continue;
            if (player.hacking < server.lackingLvl)
                continue;
            if (!server.data.hasAdminRights)
                continue;
            await tryRun(ns, () => { ns.run('/cmds/backdoor.js', 1, server.name); });
            await ns.sleep(100);
        }
    }
}
