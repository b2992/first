import {NS} from "../../index";
import {fetchServer, getLSItem, purchasables, setLSItem} from "common.js";

export async function main(ns: NS) {
  let player: any = ns.getPlayer();
  player.busy = false;
  player.sourceFiles = ns.getOwnedSourceFiles();
  // Fix when figure out how to deal with source files
  // player.busy = ns.isBusy();
  if (getLSItem('player') === undefined) {
    player.programs = [];
    player.boughtAllPrograms = false;
  } else {
    const home = await fetchServer(ns, 'home');
    player.programs = home.files.filter((f: any) => f.includes('.exe'));
    player.boughtAllPrograms = didPlayerBuyAllPrograms(player);
  }
  setLSItem('PLAYER', player);
}

function didPlayerBuyAllPrograms(player: any) {
  if (!player.tor) {
    return false;
  }
  return purchasables.every(f => player.programs.includes(f.name));
}
