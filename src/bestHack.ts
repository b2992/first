import {NS} from "../index";
import {fetchPlayer, networkMapFree} from "./common";

const maxMoneyCoefficient = 1.25
const growthCoefficient = 1.1
const minSecurityCoefficient = 2
const growthCap = 100 // because otherwise N00dles is always on the top of the list
const securityWeight = 200
const maxWeakenTime = 15 * 60 * 1000

export function calcScore(server: any) {
  // {"hackingLvl":1,"maxMoney":0,"minSecurity":1,"growth":1}
  let money = Math.pow(server.maxMoney, maxMoneyCoefficient)
  let growth = Math.pow(Math.min(server.growth, growthCap), growthCoefficient)
  let minSec = Math.pow(server.minSecurity, minSecurityCoefficient)
  let hacking = server.hackingLvl

  return (money * growth / (securityWeight + minSec * hacking))
}

export class BestHack {
  calcsRun: boolean = false;
  constructor(public ns: NS, public serverData: any) {
    this.serverData = serverData
  }

  /**
   * @param {NS} ns
   * @param {object} player
   */
  findBestPerLevel(player: any) {
    let filtered = this.scoreAndFilterServers(player)
    return filtered.reduce((prev: any, current: any) => (prev.score > current.score) ? prev : current)
  }

  /**
   * @param {NS} ns
   * @param {object} player
   */
  findTop(player: any) {
    let filtered = this.scoreAndFilterServers(player)
    return filtered.sort((a: any, b: any) => b.score - a.score)
  }

  /**
   * @param {NS} ns
   * @param {object} player
   * @param {number} count
   */
  findTopN(player: any, count: number) {
    let filtered = this.findTop(player)
    return filtered.slice(0, count)
  }

  /**
   * @param {NS} ns
   * @param {object} player
   */
  scoreAndFilterServers(player: any) {
    let scores = this.calcServerScores()
    let filtered = Object.values(scores)
      .filter((server: any) => server.hackingLvl <= player.hacking &&
        server.data.hasAdminRights &&
        server.maxMoney > 0 &&
        this.ns.formulas.hacking.weakenTime(server.data, player) < maxWeakenTime)
    return filtered
  }

  calcServerScores() {
    if (this.calcsRun) {
      return this.serverData
    }

    for (const server in this.serverData) {
      this.serverData[server].score = calcScore(this.serverData[server])
    }
    this.calcsRun = true
    return this.serverData
  }
}

export async function main(ns: NS) {
  let searcher = new BestHack(ns, await networkMapFree())
  ns.tprint( searcher.findBestPerLevel(fetchPlayer()) )
}
