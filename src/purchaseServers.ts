import {NS} from "../index";
import {Player, Server} from "./common";

export async function main(ns: NS) {
  let home = new Server(ns, 'home');
  let serverPrefix = 'pserver-';
  let ram = 8;
  if (ns.args[0]) {
    ram = parseInt(ns.args[0] as any);
  }
  let waitTime = 10;
  let serverLimit = ns.getPurchasedServerLimit();
  let purchasedServers = ns.getPurchasedServers();
  if (purchasedServers.length < serverLimit) {
    ns.tprint(`INFO Purchased Servers (${purchasedServers.length}) is less than Server Limit (${serverLimit})`)
  }
  while (purchasedServers.length < serverLimit) {
    let homeMoney = ns.getServerMoneyAvailable("home");
    let serverCost = ns.getPurchasedServerCost(ram);
    if (homeMoney > serverCost) {
      // If we have enough money, then:
      //  1. Purchase the server
      //  2. Copy our hacking script onto the newly-purchased server
      //  3. Run our hacking script on the newly-purchased server with 3 threads
      //  4. Increment our iterator to indicate that we've bought a new server
      let hostname = ns.purchaseServer(serverPrefix + (purchasedServers.length + 1), ram);
      ns.tprint(`INFO Purchased new server ${hostname} with ${ram}GB ram`);
      let purchasedServer = new Server(ns, hostname);
      await home.putScript('hack.js', hostname);
      purchasedServer.restartScript('hack.js', true);
      purchasedServers = ns.getPurchasedServers();
    } else {
      ns.tprint(`WARN Not enough Money! Waiting ${waitTime} seconds.`);
      ns.tprint(`INFO Current Money: ${homeMoney}`);
      ns.tprint(`INFO Needed Money: ${serverCost}`);
      await ns.sleep(waitTime * 1000);
    }
  }
}
