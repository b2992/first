import {NS} from "../index";

export async function main(ns: NS) {
  let purchasedServers = ns.getPurchasedServers();
  if (purchasedServers.length > 0) {
    purchasedServers.forEach((purchasedServer) => {
      ns.deleteServer(purchasedServer)
    });
    ns.tprint(`INFO ${purchasedServers.length} Purchased servers deleted!`)
  }
}
