import {NS} from "../index";
import {NetworkMapper} from "./common";

/**
 * @param {NS} ns
 **/
export async function main(ns: NS) {
  let mapper = new NetworkMapper(ns);

  ns.print(`Writing networkMap to local storage and ${mapper.filename}!`);
  await mapper.writeMap();
}
