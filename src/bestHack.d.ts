import { NS } from "../index";
export declare function calcScore(server: any): number;
export declare class BestHack {
    ns: NS;
    serverData: any;
    calcsRun: boolean;
    constructor(ns: NS, serverData: any);
    /**
     * @param {NS} ns
     * @param {object} player
     */
    findBestPerLevel(player: any): unknown;
    /**
     * @param {NS} ns
     * @param {object} player
     */
    findTop(player: any): unknown[];
    /**
     * @param {NS} ns
     * @param {object} player
     * @param {number} count
     */
    findTopN(player: any, count: number): unknown[];
    /**
     * @param {NS} ns
     * @param {object} player
     */
    scoreAndFilterServers(player: any): unknown[];
    calcServerScores(): any;
}
export declare function main(ns: NS): Promise<void>;
