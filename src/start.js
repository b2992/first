import { GameController, Internet, Player } from "./common.js";
export async function main(ns) {
    ns.tprint('Start script running');
    let internet = new Internet(ns);
    internet.scan();
    ns.tprint(`Internet Got ${internet.getRoot()} Root of ${internet.count()} servers`);
    await internet.updateHack();
    let player = new Player(ns);
    ns.tprint('Start script completed!');
    ns.tprint('Starting Game Controller');
    let gameController = new GameController(ns);
    ns.tprint('Initializing Game Controller');
    await gameController.init();
    // let home = new Server(ns, 'home');
    // let servers = home.scan();
    // await servers.loadServers();
}
