import {NS, Player as PlayerInfo, Server as ServerInfo} from "../index";

export const factionServers = {
  "CSEC"         : "CyberSec",
  "avmnite-02h"  : "NiteSec",
  "I.I.I.I"      : "The Black Hand",
  "run4theh111z" : "BitRunners",
  "The-Cave"     : "Daedalus",
}

export const rootFiles = [
  { name: "BruteSSH.exe", cost: 500000, },
  { name: "FTPCrack.exe", cost: 1500000, },
  { name: "relaySMTP.exe", cost: 5000000, },
  { name: "HTTPWorm.exe", cost: 30000000, },
  { name: "SQLInject.exe", cost: 250000000, },
]

export const purchasables = rootFiles.concat([
  // { name: "Formulas.exe", cost: 5000000000, }
])

export const gangEquipment = {
  weapons  : ["Baseball Bat","Katana","Glock 18C","P90C","Steyr AUG","AK-47","M15A10 Assault Rifle","AWM Sniper Rifle"],
  armor    : ["Bulletproof Vest","Full Body Armor","Liquid Body Armor","Graphene Plating Armor"],
  vehicles : [ "Ford Flex V20","ATX1070 Superbike","Mercedes-Benz S9001","White Ferrari"],
  rootkits : ["NUKE Rootkit","Soulstealer Rootkit","Demon Rootkit","Hmap Node","Jack the Ripper"],
  hackAugs   : ["BitWire","Neuralstimulator","DataJack"],
  combatAugs : ["Bionic Arms","Bionic Legs","Bionic Spine","BrachiBlades","Nanofiber Weave","Synthetic Heart","Synfibril Muscle","Graphene Bone Lacings"],
}

export const lsKeys = {
  NMAP : 'network_map',
  PLAYER : 'player',
  RESERVE : 'reserve',
  BITNODE : 'bn_multipliers',
  SOURCEFILES : 'owned_sourcefiles',
  WORKING : 'working',
  DECOMMISSIONED : 'decommissioned',
  HACKPERCENT : 'hack_percent',
  CLASHTIME : 'next_territory_warefare',
  GANGMETA : 'gang_information',
  SLEEVEMETA : 'sleeve_information',
}

export interface CachedDataOptions {
  localStorage: boolean,
  localStorageFrequency: number,
  file?: string,
  fileFrequency: number,
  data?: any
}
export class CachedData {
  _key: string = "";
  _data: any = null;
  lastLSUpdate = 0;
  lastFSUpdate = 0;
  lastUpdate = 0;
  constructor(public ns: NS, key: string, public options: CachedDataOptions) {
    this.key = key;
    if (options.data) {
      this.data = options.data;
    }
  }
  load() {
    let stored = null;
    if (this.options.localStorage === true && this.options.file) {
      let lsStored = this.loadFromLocalStorage();
      let fsStored = this.loadFromFile();
      stored = (lsStored.lastUpdate > fsStored.lastUpdate ? lsStored : fsStored);
    }
    else if (this.options.localStorage === true) {
      stored = this.loadFromLocalStorage();
    }
    else if (this.options.file) {
      stored = this.loadFromFile();
    }
    if (stored !== null) {
      this._data = stored.data;
    }
    return this;
  }
  async sync() {
    let stored = null;
    if (this.options.localStorage === true && this.options.file) {
      let lsStored = this.loadFromLocalStorage();
      let fsStored = this.loadFromFile();
      stored = (lsStored.lastUpdate > fsStored.lastUpdate ? lsStored : fsStored);
    }
    else if (this.options.localStorage === true) {
      stored = this.loadFromLocalStorage();
    }
    else if (this.options.file) {
      stored = this.loadFromFile();
    }
    if (stored !== null) {
      if (this.lastUpdate > stored.lastUpdate) {
        await this.save();
      } else {
        this._data = stored.data;
      }
    }
    return this;
  }
  loadFromLocalStorage() {
    let item = localStorage.getItem(this.key);
    return item ? JSON.parse(item) : undefined;
  }
  loadFromFile() {
    let item = this.ns.read(`${this.options.file}.txt`);
    return item ? JSON.parse(item) : undefined;
  }
  get key() {
    return this._key;
  }
  set key(key) {
    this._key = key;
  }
  get data() {
    return this._data;
  }
  async updateData(data: any) {
    let oldData = this._data;
    this._data = data;
    await this.onChange(oldData, data);
    this.lastUpdate = Date.now();
  }
  set data(data) {
    let oldData = this._data;
    this._data = data;
    this.onChange(oldData, data).then(() => {
      this.lastUpdate = Date.now();
    }).catch((err) => {
      this.ns.tprint(err);
    });
  }
  async save() {
    if (this.options.localStorage === true) {
      localStorage.setItem(this.key, JSON.stringify({data: this.data, lastUpdate: Date.now()}));
    }
    if (this.options.file) {
      await this.ns.write(`${this.options.file}.txt`, [JSON.stringify({
        data: this.data,
        lastUpdate: Date.now()
      })], 'w');
    }
  }
  async onChange(oldData: any, newData: any) {
    let now = Date.now();
    if (this.options.localStorage === true) {
      if (now > this.lastLSUpdate + this.options.localStorageFrequency) {
        this.lastLSUpdate = now;
        localStorage.setItem(this.key, JSON.stringify({data: newData, lastUpdate: this.lastLSUpdate}));
      }
    }
    if (this.options.file) {
      if (now > this.lastFSUpdate + this.options.fileFrequency) {
        this.lastFSUpdate = now;
        await this.ns.write(`${this.options.file}.txt`, [JSON.stringify({data: newData, lastUpdate: this.lastFSUpdate})], 'w');
      }
    }
  }
}

export class Internet {
  servers: { [id: string]: Server } = {};
  names: string[] = [];
  constructor(public ns: NS) {
    this.add('home');
  }
  scan() {
    this.scanServers(this.servers.home.scan());
  }
  scanServers(servers: Servers) {
    let me = this;
    servers.names.map((name) => {
      if (me.names.indexOf(name) === -1) {
        me.ns.print(`Scanning Server: ${name}`);
        me.add(name, servers.servers[name]);
        me.scanServers(me.servers[name].scan());
      }
    });
  }

  add(name: string, server: Server = new Server(this.ns, name)): Server {
    if (this.servers[name]) {
      return this.servers[name];
    } else {
      this.servers[name] = server;
      this.names.push(name);
      return server;
    }
  }
  get (name: string): Server {
    return this.servers[name];
  }
  remove(name: string): boolean {
    if (this.servers[name]) {
      delete this.servers[name];
      return true;
    } else {
      return false;
    }
  }
  count(): number {
    return this.names.length;
  }

  getRoot(recheck: boolean = false): number {
    let me = this;
    let i = 0;
    me.names.forEach((name) => {
      let hasRoot = me.servers[name].hasRootAccess(recheck);
      if (hasRoot) {
        i++;
      }
    });
    return i;
  }
  async updateHack() {
    let me = this;
    for (let name of me.names) {
      if (name !== 'home') {
        let home = me.servers['home'];
        let server = me.servers[name];
        if (server.hasRootAccess() === true) {
          let putScript = await home.putScript('hack.js', server.name);
          me.ns.print(`${(putScript?"INFO":"WARN")} Adding Hack to ${server.name}: ${putScript}`);
          let restart = server.restartScript('hack.js', true);
          me.ns.print(`${(restart?"INFO":"WARN")} Restarting Hack on ${server.name}: ${restart}`);
        }
      }
    }
  }
}

export class Servers {
  servers: { [id: string]: Server } = {};
  constructor(public ns: NS, public names: string[]) {
    let me = this;
    names.forEach((name) => {
      me.servers[name] = new Server(ns, name);
    });
  }

  // async loadServers() {
  //   let me = this;
  //   let servers = me.ns.scan();
  //   me.ns.print(`Servers: ${JSON.stringify(servers, null, 2)}`)
  //   for (let s of servers) {
  //     let server = new Server(me.ns, s);
  //     me.ns.print(`Server(${s}) Loading`);
  //     me.servers.push(server);
  //     let rooted = server.getRoot();
  //     if (rooted) {
  //       me.ns.print(`Server(${s}): Installing Hack`);
  //       // me.ns.kill('hack.js', s);
  //       await me.ns.scp('hack.js', 'home', s);
  //       me.ns.print(`Server(${s}): Getting Max Threads`);
  //       let maxThreads = server.getScriptMaxThreads('hack.js');
  //       me.ns.print(`Server(${s}): Starting Hack with ${maxThreads} threads`);
  //       me.ns.exec('hack.js', s, maxThreads);
  //       // me.ns.print(`Server(${s}) has root`)
  //     }
  //     // server.getScriptMaxThreads(hackScript);
  //   }
  // }
}

export class Server {
  servers: Servers | null = null;
  moneyAvailable: number | null = null;
  maxMoney: number | null = null;
  securityLevel: number | null = null;
  minSecurityLevel: number | null = null;
  requiredHackingLevel: number | null = null;
  numPortsRequired: number | null = null;
  maxRam: number | null = null;
  usedRam: number | null = null;
  info: ServerInfo | null = null;
  constructor(public ns: NS, public name: string) {
    let me = this;
    me.getServerSecurityLevel();
    me.getServerMinSecurityLevel();
    me.getServerRequiredHackingLevel();
    me.getServerNumPortsRequired();
    me.getServerMaxRam();
    me.getServerUsedRam();
    me.getInfo();
  }
  scan(recheck: boolean = false) {
    let me = this;
    if (me.servers === null || recheck === true) {
      me.servers = new Servers(me.ns, me.ns.scan(me.name));
    }
    return me.servers;
  }
  hasRootAccess(recheck: boolean = false) {
    let me = this;
    let server = me.name;
    let myHackingLevel = me.ns.getHackingLevel();
    let hasRoot = me.ns.hasRootAccess(server);
    // me.ns.print(`INFO Server(${server}): has root ${hasRoot}`);
    if (hasRoot === false) {
      let requiredLevel = me.getServerRequiredHackingLevel(recheck);
      if (requiredLevel <= myHackingLevel) {
        let portsOpened = 0;
        let portsRequired = me.getServerNumPortsRequired(recheck);
        if (portsRequired > portsOpened && me.ns.fileExists("BruteSSH.exe", "home")) {
          me.ns.brutessh(server);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("FTPCrack.exe", "home")) {
          me.ns.ftpcrack(server);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("relaySMTP.exe", "home")) {
          me.ns.relaysmtp(server);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("HTTPWorm.exe", "home")) {
          me.ns.httpworm(server);
          portsOpened++;
        }
        if (portsRequired > portsOpened && me.ns.fileExists("SQLInject.exe", "home")) {
          me.ns.sqlinject(server);
          portsOpened++;
        }
        if (portsOpened >= portsRequired) {
          // if (portsOpened > 0) {
          //   me.ns.print(`INFO Server(${server}): Opened ${portsOpened}/${portsRequired} ports`);
          // }
          // me.ns.print(`INFO Server(${server}): Rooting`);
          me.ns.nuke(server);
          // me.ns.print(`INFO Server(${server}): Rooted`);
          return true;
        } else {
          return false;
        }
      } else {
        // me.ns.print(`INFO Server(${server}): Required Level ${requiredLevel}`);
        return false;
      }
    } else {
      return true;
    }
  }
  hasScript(script: string) {
    return this.ns.fileExists(script, this.name);
  }
  async putScript(script: string, destination: string) {
    return await this.ns.scp(script, this.name, destination);
  }
  restartScript(script: string, maxThreads: boolean = true): number {
    let me = this;
    if (me.ns.scriptRunning(script, me.name)) {
      me.ns.scriptKill(script, me.name);
    }
    if (maxThreads === true) {
      let maxThreads = me.getScriptMaxThreads(script, true);
      if (maxThreads > 0) {
        return me.ns.exec(script, me.name, maxThreads);
      } else {
        return 0;
      }
    } else {
      return me.ns.exec(script, me.name);
    }
  }
  getScriptMaxThreads(script: string, recheck: boolean = false) {
    let me = this;
    let name = me.name;
    let scriptMem = me.ns.getScriptRam(script, name);
    me.ns.print(`INFO Script(${script}) Memory: ${scriptMem}`);
    let serverMaxRam = me.getServerMaxRam(recheck);
    let serverUsedRam = me.getServerUsedRam(recheck);
    let serverFreeRam = serverMaxRam - serverUsedRam;
    let scriptMaxThreads = Math.floor(serverFreeRam / scriptMem);
    me.ns.print(`INFO Server(${name}): has ${serverFreeRam} free memory`);
    me.ns.print(`INFO Server(${name}): can run ${scriptMaxThreads} threads`);
    return scriptMaxThreads;
  }

  getServerMoneyAvailable(recheck: boolean = false): number {
    let me = this;
    if (me.moneyAvailable === null || recheck === true) {
      me.moneyAvailable = me.ns.getServerMoneyAvailable(me.name);
    }
    return me.moneyAvailable;
  }
  getServerMaxMoney(recheck: boolean = false): number {
    let me = this;
    if (me.maxMoney === null || recheck === true) {
      me.maxMoney = me.ns.getServerMaxMoney(me.name);
    }
    return me.maxMoney;
  }
  getServerSecurityLevel(recheck: boolean = false): number {
    let me = this;
    if (me.securityLevel === null || recheck === true) {
      me.securityLevel = me.ns.getServerSecurityLevel(me.name);
    }
    return me.securityLevel;
  }
  getServerMinSecurityLevel(recheck: boolean = false): number {
    let me = this;
    if (me.minSecurityLevel === null || recheck === true) {
      me.minSecurityLevel = me.ns.getServerMinSecurityLevel(me.name);
    }
    return me.minSecurityLevel;
  }
  getServerRequiredHackingLevel(recheck: boolean = false): number {
    let me = this;
    if (me.requiredHackingLevel === null || recheck === true) {
      me.requiredHackingLevel = me.ns.getServerRequiredHackingLevel(me.name);
    }
    return me.requiredHackingLevel;
  }
  getServerNumPortsRequired(recheck: boolean = false): number {
    let me = this;
    if (me.numPortsRequired === null || recheck === true) {
      me.numPortsRequired = me.ns.getServerNumPortsRequired(me.name);
    }
    return me.numPortsRequired;
  }
  getServerMaxRam(recheck: boolean = false): number {
    let me = this;
    if (me.maxRam === null || recheck === true) {
      me.maxRam = me.ns.getServerMaxRam(me.name);
    }
    return me.maxRam;
  }
  getServerUsedRam(recheck: boolean = false): number {
    let me = this;
    if (me.usedRam === null || recheck === true) {
      me.usedRam = me.ns.getServerUsedRam(me.name);
    }
    return me.usedRam;
  }
  getInfo(recheck: boolean = false): ServerInfo {
    let me = this;
    if (me.info === null || recheck === true) {
      me.info = me.ns.getServer(me.name);
    }
    return me.info;
  }
}

export class Player {
  hackingLevel: number | null = null;
  hackingMultipliers: any = null;
  hacknetMultipliers: any = null;
  info: PlayerInfo | null = null;
  constructor(public ns: NS) {
    ns.hacknet.numNodes();
    this.info = ns.getPlayer();
  }
  getHackingLevel(recheck: boolean = false) {
    let me = this;
    if (me.hackingLevel === null || recheck === true) {
      me.hackingLevel = me.ns.getHackingLevel();
    }
    return me.hackingLevel;
  }
  getHackingMultipliers(recheck: boolean = false) {
    let me = this;
    if (me.hackingMultipliers === null || recheck === true) {
      me.hackingMultipliers = me.ns.getHackingMultipliers();
    }
    return me.hackingMultipliers;
  }
  getHacknetMultipliers(recheck: boolean = false) {
    let me = this;
    if (me.hacknetMultipliers === null || recheck === true) {
      me.hacknetMultipliers = me.ns.getHacknetMultipliers();
    }
    return me.hacknetMultipliers;
  }
}

/**
 * @param {NS} ns
 **/
export class NetworkMapper {
  filename: string = 'network_map.txt';
  serverData: any = {};
  serverList: string[] = ['home'];
  constructor(public ns: NS) {
    ns.print("Initializing new Network object");
    this.serverData['home'] = this.aggregateData('home', '');
    this.serverList = ['home'];
    this.walkServers();
  }

  walkServers() {
    let me = this;
    for (let i = 0; i < me.serverList.length; i++) {
      me.ns.scan(me.serverList[i]).forEach(function (host) {
        if (!me.serverList.includes(host)) {
          me.serverData[host] = me.aggregateData(host, me.serverList[i]);
          me.serverList.push(host);
        }
      }, me);
    }
    return me.serverData;
  }

  async writeMap() {
    let me = this;
    setLSItem('NMAP', me.serverData)

    let line = "Name,MaxRam,PortsRequired," +
      "HackingLvl,MaxMoney,MinSecurity,Growth," +
      "Parent\r\n";
    await me.ns.write(me.filename, line as any, "w");

    let data = me.serverList.map(function (server) {
      return Object.values(me.serverData[server]).join(",");
    }, me);
    await me.ns.write(me.filename, data.join("\r\n") as any, "a");
    return;
  }

  aggregateData(server: string, parent: string) {
    let sobj = {
      name: server,
      maxRam: this.ns.getServerMaxRam(server),
      portsRequired: this.ns.getServerNumPortsRequired(server),
      hackingLvl: this.ns.getServerRequiredHackingLevel(server),
      maxMoney: this.ns.getServerMaxMoney(server),
      minSecurity: this.ns.getServerMinSecurityLevel(server),
      growth: this.ns.getServerGrowth(server),
      parent: parent,
    };
    updateData(this.ns, sobj);
    return sobj;
  }
}

export class GameController {
  jobs: {name: string, file: string, frequency: number, last: number}[] = [
  ];
  running: boolean = true;
  constructor(public ns: NS) {
    const sec = 1000;
    const min = 60 * sec;
    this.jobs.push({
      name: 'Player Monitor',
      file: '/jobs/playerMonitor.js',
      frequency: 20,
      last: 0
    });
    this.jobs.push({
      name: 'Servers Monitor',
      file: '/jobs/serversMonitor.js',
      frequency: 0,
      last: 0
    });
    this.jobs.push({
      name: 'Backdoor Monitor',
      file: '/jobs/backdoorMonitor.js',
      frequency: 6 * sec,
      last: 0
    });
    this.jobs.push({
      name: 'Nuker',
      file: '/jobs/nuker.js',
      frequency: 7 * sec,
      last: 0
    });
  }
  async init() {
    let me = this;
    while (me.running) {
      await me.executeJobs();
    }
  }
  async executeJobs() {
    let me = this;
    let first = true;
    let proc = null;
    for (let job of me.jobs) {
      proc = me.ns.ps('home').find(p => p.filename == job.file);
      if (!proc && Date.now() > job.last + job.frequency) {
        await tryRun(me.ns, () => me.ns.run(job.file, 1));
        job.last = Date.now();
      }
      if (first) {
        await me.ns.sleep(50);
        first = false;
      }
    }
    await me.ns.sleep(5);
  }
}






export function updateData(ns: NS, server: any) {
  try {
    server.data = ns.getServer(server.name)
    server.files = ns.ls(server.name)
    server.security = ns.getServerSecurityLevel(server.name)
    // max ram changes sometimes w/ home, purchased servers
    server.maxRam = ns.getServerMaxRam(server.name)
  } catch(e: any) { ns.print(e.message) }
  return server
}


/**
 * @param {integer} ms - milliseconds to sleep
 * @cost 0 GB
 */
export function mySleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * @param {NS} ns
 * @cost 0.1 GB
 * @returns {integer} player's money available
 */
export function myMoney(ns: NS) {
  return ns.getServerMoneyAvailable('home')
}

/**
 * @param {NS} ns
 * @param {number} cost - amount wanted
 * @cost 0.2 GB
 */
export async function waitForCash(ns: NS, cost: number) {
  if ((myMoney(ns) - reserve(ns)) >= cost) {
    ns.print("I have enough: " + ns.nFormat(cost, "$0.000a"))
    return;
  }
  ns.print("Waiting for " + ns.nFormat(cost + reserve(ns), "$0.000a"))
  while ((myMoney(ns) - reserve(ns)) < cost) {
    await ns.sleep(3000)
  }
}

/**
 * Reserve a certain amount for big purchases
 * You can manually reserve an amount by setting a number in localStorage.
 *     run lsSet.js reserve 4.5e9
 *
 * @param {NS} ns
 * @cost 0.1 GB
 */
export function reserve(ns: NS) {
  let manualReserve = Number(getLSItem('reserve') || 0)
  for ( const file of purchasables ) {
    if (!ns.fileExists(file.name, 'home')) {
      return file.cost + manualReserve
    }
  }
  return manualReserve
}

/**
 * @param {string} key
 * @return {any} The value read from localStorage
 * @cost 0 GB
 **/
export function getLSItem(key: string) {
  // @ts-ignore
  let item = localStorage.getItem(lsKeys[key.toUpperCase()]);
  return item ? JSON.parse(item) : undefined
}

/**
 * @param {string} key
 * @param {any} value
 * @cost 0 GB
 **/
export function setLSItem(key: string, value: any) {
  // @ts-ignore
  localStorage.setItem(lsKeys[key.toUpperCase()], JSON.stringify(value))
}

/**
 * @param {string} key
 * @cost 0 GB
 **/
export function clearLSItem(key: string) {
  // @ts-ignore
  localStorage.removeItem(lsKeys[key.toUpperCase()])
}

/**
 * @return {object} The player data from localStorage
 * @cost 0 GB
 **/
export function fetchPlayer() {
  return getLSItem('player');
}


/**
 * @returns {bool} access from a specified source file
 * @param {number} num - which source file we want to know about
 * @param {number} level (optional) - sometimes the specific functionality
 *                       requires the bitnode to be leveled up.
 * @cost 0 GB
 */
export function haveSourceFile(num: number, level: number = 1) {
  if ( fetchPlayer().bitNodeN == num )
    return true

  let ownedSourceFiles = getLSItem('sourceFiles')
  return ownedSourceFiles.some((sf: { n: number; lvl: number; }) => sf.n == num && sf.lvl >= level )
}

/**
 * @returns {number} number of exe rootfiles on the player's computer
 * @cost 0 GB
 */
export function toolsCount() {
  let player = getLSItem('player')
  return (rootFiles.filter((file) => player.programs.includes(file.name))).length
}

/**
 * @param {NS} ns
 * @param {array} listOfLogs - list of loggable functions to disable
 * @cost 0 GB
 */
export function disableLogs(ns: NS, listOfLogs: any[]) {
  ['disableLog'].concat(...listOfLogs).forEach(log => ns.disableLog(log));
}

/**
 * @param {NS} ns
 * @param {function} callback
 * @cost 0 GB
 */
export async function tryRun(ns: NS, callback: Function) {
  let pid = callback()
  while (pid == 0) {
    await ns.sleep(30)
    pid = callback()
  }
  return pid
}


/**
 * @param {NS} ns
 * @param {string} log
 * @param {string} toastVariant
 * @cost 0 GB
 * Prints a message, and also toasts it!
 */
export function announce(ns: NS, log: string, toastVariant = 'info') {
  // If an error is caught because the script is killed, ns becomes undefined
  checkNsInstance(ns);

  ns.print(`${toastVariant.toUpperCase()}: ${log}`);
  ns.toast(log, toastVariant.toLowerCase());
}


// yoink: https://gist.github.com/robmathers/1830ce09695f759bf2c4df15c29dd22d
/**
 * @param {array} data is an array of objects
 * @param {string|function} key is the key, property accessor, or callback
 *                          function to group by
 **/
export function groupBy(data: any[], key: string | Function) {
  // reduce runs this anonymous function on each element of `data`
  // (the `item` parameter, returning the `storage` parameter at the end
  return data.reduce(function(storage, item) {
    // get the first instance of the key by which we're grouping
    var group = key instanceof Function ? key(item) : item[key];

    // set `storage` for this instance of group to the outer scope (if not
    // empty) or initialize it
    storage[group] = storage[group] || [];

    // add this item to its group within `storage`
    storage[group].push(item);

    // return the updated storage to the reduce function,
    //which will then loop through the next
    return storage;
  }, {}); // {} is the initial value of the storage
};


// All of the below functions are stolen & reformatted/refactored from Insight's
// helper. They are required to make his scripts work.

/**
 * Return a formatted representation of the monetary amount using scale sympols
 * (e.g. $6.50M)
 * @param {number} num - The number to format
 * @param {number=} maxSigFigures - (default: 6) The maximum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} maxDecimalPlaces - (default: 3) The maximum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export function formatMoney(num: number, maxSigFigures = 6, maxDecimalPlaces = 3) {
  let numberShort = formatNumberShort(num, maxSigFigures, maxDecimalPlaces)
  return num >= 0 ? "$" + numberShort : numberShort.replace("-", "-$")
}

/**
 * Return a formatted representation of the monetary amount using scale sympols
 * (e.g. 6.50M)
 * @param {number} num - The number to format
 * @param {number=} maxSigFigures - (default: 6) The maximum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} maxDecimalPlaces - (default: 3) The maximum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export function formatNumberShort(num: number, maxSigFigures = 6, maxDecimalPlaces = 3) {
  const symbols = ["", "k", "m", "b", "t", "qa", "qi", "sx", "sp", "oc", "e30",
    "e33", "e36", "e39"]
  const sign = Math.sign(num) < 0 ? "-" : ""
  for (var i = 0, num = Math.abs(num); num >= 1000 && i < symbols.length; i++) {
    num /= 1000
  }
  const sigFigs = maxSigFigures - Math.floor(1 + Math.log10(num))
  const fixed = num.toFixed(Math.max(0, Math.min(maxDecimalPlaces, sigFigs)))
  return sign + fixed + symbols[i]
}

/**
 * Return a number formatted with the specified number of significatnt figures
 * or decimal places, whichever is more limiting.
 * @param {number} num - The number to format
 * @param {number=} minSigFigures - (default: 6) The minimum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} minDecimalPlaces - (default: 3) The minimum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export function formatNumber(num: number, minSigFigures = 3, minDecimalPlaces = 1) {
  if ( num == 0.0 )
    return  num

  let sigFigs = Math.max(0, minSigFigures - Math.ceil(Math.log10(num)))
  return num.toFixed(Math.max(minDecimalPlaces, sigFigs))
}

/**
 * Formats some RAM amount as a round number of GB with thousands separators
 * e.g. `1,028 GB`
 * @param {number} n - the number to format
 */
export function formatRam(n: number) {
  if (n < 1e3) return formatNumber(n, 3, 0) + 'GB'
  if (n < 1e6) return formatNumber(n / 1e3, 3, 0) + 'TB'
  if (n < 1e9) return formatNumber(n / 1e6, 3, 0) + 'PB'
  if (n < 1e12) return formatNumber(n / 1e9,3, 0) + 'EB'
  return `${Math.round(n).toLocaleString()} GB`;
}

/**
 * Format a duration (in milliseconds) as e.g. '1h 21m 6s' for big durations or
 * e.g '12.5s' / '23ms' for small durations
 **/
export function formatDuration(duration: number) {
  if (duration < 1000) return `${duration.toFixed(0)}ms`
  const portions = [];
  const msInHour = 1000 * 60 * 60;
  const hours = Math.trunc(duration / msInHour);
  if (hours > 0) {
    portions.push(hours + 'h');
    duration -= (hours * msInHour);
  }
  const msInMinute = 1000 * 60;
  const minutes = Math.trunc(duration / msInMinute);
  if (minutes > 0) {
    portions.push(minutes + 'm');
    duration -= (minutes * msInMinute);
  }
  let seconds = (duration / 1000.0)
  // Include millisecond precision if we're on the order of seconds
  seconds = Number((hours == 0 && minutes == 0) ? seconds.toPrecision(3) : seconds.toFixed(0));
  if (seconds > 0) {
    portions.push(seconds + 's');
    duration -= (minutes * 1000);
  }
  return portions.join(' ');
}

/** Generate a hashCode for a string that is pretty unique most of the time */
export function hashCode(s: string) {
  return s.split("").reduce(function (a, b) {
    a = ((a << 5) - a) + b.charCodeAt(0)
    return a & a
  }, 0)
}

// FUNCTIONS THAT PROVIDE ALTERNATIVE IMPLEMENTATIONS TO EXPENSIVE NS FUNCTIONS
// VARIATIONS ON NS.RUN

/**
 * @param {NS} ns
 *  Use where a function is required to run a script and you have already
 * referenced ns.run in your script
 **/
export function getFnRunViaNsRun(ns: NS) { return checkNsInstance(ns).run; }

/**
 * @param {NS} ns
 * @param {string} host
 * Use where a function is required to run a script and you have already
 * referenced ns.exec in your script
 **/
export function getFnRunViaNsExec(ns: NS, host = "home") {
  checkNsInstance(ns);
  return function (scriptPath: string, ...args: any) {
    return ns.exec(scriptPath, host, ...args)
  }
}
// VARIATIONS ON NS.ISRUNNING

/**
 * @param {NS} ns
 * Use where a function is required to check if a script is running and you have
 * already referenced ns.isRunning in your script
 **/
export function getFnIsAliveViaNsIsRunning(ns: NS) {
  return checkNsInstance(ns).isRunning
}

/**
 * @param {NS} ns
 * Use where a function is required to check if a script is running and you have
 * already referenced ns.ps in your script
 **/
export function getFnIsAliveViaNsPs(ns: NS) {
  checkNsInstance(ns);
  return function (pid: number, host: string | undefined) {
    return ns.ps(host).some(process => process.pid === pid)
  }
}

/**
 * Evaluate an arbitrary ns command by writing it to a new script and then
 * running or executing the new file
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                  run as a new script.
 */
export async function runCommand(ns: NS, command: string, fileName: string, verbose: boolean, ...args: any[]) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['run', 'sleep'])
  return await runCommand_Custom(ns, ns.run, command,fileName, verbose, ...args)
}

/**
 * Evaluate an arbitrary ns command by writing it to a new script, running the
 * script, then waiting for it to complete running.
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                           desired result (e.g. "ns.exec('nuker.js', 'home')")
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                             of the file to which data will be written to disk
 *                             by a temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                          result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                         run as a new script
 */
export async function runCommandAndWait(ns: NS, command: string, fileName: string, verbose: boolean, ...args: any[]) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['run', 'sleep'])

  const pid = await runCommand_Custom(ns,ns.run,command,fileName,verbose,...args)
  if (pid === 0) {
    throw (`runCommand returned no pid. (Insufficient RAM, or bad command?) ` +
      `Destination: ${fileName} Command: ${command}`)
  }
  await waitForProcessToComplete_Custom(ns, ns.isRunning, pid, verbose)
}

/**
 * An advanced version of runCommand that lets you pass your own "isAlive" test
 * to reduce RAM requirements (e.g. to avoid referencing ns.isRunning)
 *
 * Importing incurs 0 GB RAM (assuming fnRun, fnWrite are implemented using
 * another ns function you already reference elsewhere like ns.exec)
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {function} fnRun - A single-argument function used to start the new
 *                   script, e.g. `ns.run` or
 *                   `(f,...args) => ns.exec(f, "home", ...args)`
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                  run as a new script.
 **/
export async function runCommand_Custom(ns: NS, fnRun: Function, command: string, fileName: string, verbose: boolean, ...args: any[]) {
  checkNsInstance(ns)
  const helpers = [
    'mySleep', 'toolsCount', 'myMoney', 'waitForCash', 'reserve',
    'tryRun', 'getLSItem', 'setLSItem', 'clearLSItem', 'fetchPlayer',
    'announce', 'groupBy', 'formatMoney', 'formatNumberShort', 'formatNumber',
    'formatDuration', 'formatRam', 'hashCode',
  ]
  const script =
    // `import { ${helpers.join(', ')} } fr` + `om 'helpers.js';\n` +
    // `import { networkMap, fetchServer } fr` + `om 'network.js';\n` +
    // `import * as constants fr` + `om 'constants.js';\n` +
    `export async function main(ns) { try { ` +
    (verbose ? `let output = ${command}; ns.tprint(output)` : command) +
    `; } catch(err) { ns.tprint(String(err)); throw(err); } }`;
  fileName = fileName || `/Temp/${hashCode(command)}-command.js`;
  // To improve performance and save on garbage collection, we can skip
  // writing this exact same script was previously written (common for
  // repeatedly-queried data)
  if (ns.read(fileName) != script) {
    await ns.write(fileName, script as any, "w")
  }
  return fnRun(fileName, ...args)
}

/**
 * Wait for a process id to complete running
 * Importing incurs a maximum of 0.1 GB RAM (for ns.isRunning)
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {int} pid - The process id to monitor
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 **/
export async function waitForProcessToComplete(ns: NS, pid: number, verbose: boolean) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['isRunning'])
  return await waitForProcessToComplete_Custom(ns, ns.isRunning, pid, verbose)
}
/**
 * An advanced version of waitForProcessToComplete that lets you pass your own
 * "isAlive" test to reduce RAM requirements (e.g. to avoid referencing
 * ns.isRunning)
 *
 * Importing incurs 0 GB RAM (assuming fnIsAlive is implemented using another ns
 * function you already reference elsewhere like ns.ps)
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {function} fnIsAlive - A single-argument function used to test, e.g.
 *                   `ns.isRunning` or
 *                   `pid => ns.ps("home").some(process => process.pid === pid)`
 * @param {number} pid
 * @param {bool} verbose
 **/
export async function waitForProcessToComplete_Custom(ns: NS, fnIsAlive: Function, pid: number, verbose: boolean) {
  checkNsInstance(ns);
  if (!verbose) disableLogs(ns, ['sleep']);
  // Wait for the PID to stop running (cheaper than e.g. deleting (rm) a
  // possibly pre-existing file and waiting for it to be recreated)
  for (var retries = 0; retries < 1000; retries++) {
    if (!fnIsAlive(pid)) break; // Script is done running
    if (verbose && retries % 100 === 0) {
      ns.print(`Waiting for pid ${pid} to complete... (${retries})`)
    }
    await ns.sleep(10);
  }
  // Make sure that the process has shut down and we haven't just stopped retrying
  if (fnIsAlive(pid)) {
    let error = `run-command pid ${pid} is running much longer than expected. `+
      `Max retries exceeded.`
    ns.print(error)
    throw error
  }
}

/**
 * Retrieve the result of an ns command by executing it in a temporary .js
 * script, writing the result to a file, then shuting it down
 *
 * Importing incurs a maximum of 1.1 GB RAM (0 GB for ns.read, 1 GB for ns.run,
 * 0.1 GB for ns.isRunning).
 *
 * Has the capacity to retry if there is a failure (e.g. due to lack of RAM
 * available). Not recommended for performance-critical code.
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 * @param {integer} maxRetries - (default 5) How many times to retry for not
 *                  more RAM before throwing an error
 * @param {integer} retryDelayMs - (default 50) How many milliseconds to wait
 *                  before retrying
 **/
export async function getNsDataThroughFile(ns: NS, command: string, fName: string, verbose: boolean, maxRetries = 5, retryDelayMs = 50) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['run', 'isRunning'])
  return await getNsDataThroughFile_Custom(ns,
    ns.run,
    ns.isRunning,
    command,
    fName,
    verbose,
    maxRetries,
    retryDelayMs)
}
/**
 * An advanced version of getNsDataThroughFile that lets you pass your own
 * "fnRun" and "fnIsAlive" implementations to reduce RAM requirements
 *
 * Importing incurs no RAM (now that ns.read is free) plus whatever fnRun /
 * fnIsAlive you provide it
 *
 * Has the capacity to retry if there is a failure (e.g. due to lack of RAM
 * available). Not recommended for performance-critical code.
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {function} fnRun - A single-argument function used to start the new
 *                   script, e.g. `ns.run` or
 *                   `(f,...args) => ns.exec(f, "home", ...args)`
 * @param {function} fnIsAlive - A single-argument function used to test if the
 *                   script has completed, e.g. `ns.isRunning` or
 *                   `pid => ns.ps("home").some(process => process.pid === pid)`
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 * @param {integer} maxRetries - (default 5) How many times to retry for not
 *                  more RAM before throwing an error
 * @param {integer} retryDelayMs - (default 50) How many milliseconds to wait
 *                  before retrying
 **/
export async function getNsDataThroughFile_Custom(ns: NS, fnRun: Function, fnIsAlive: Function, command: string, fName: string, verbose: boolean, maxRetries = 5, retryDelayMs = 50) {
  checkNsInstance(ns);
  if (!verbose) disableLogs(ns, ['read'])
  const commandHash = hashCode(command)
  fName = fName || `/Temp/${commandHash}-data.txt`
  const fNameCommand = (fName || `/Temp/${commandHash}-command`) + '.js'
  // Prepare a command that will write out a new file containing the results of
  // the command unless it already exists with the same contents
  // (saves time/ram to check first)
  const commandToFile = `const result = JSON.stringify(${command}); ` +
    `if (ns.read("${fName}") != result) await ns.write("${fName}", result, 'w')`
  while (maxRetries-- > 0) {
    try {
      const pid = await runCommand_Custom(ns, fnRun, commandToFile, fNameCommand, false)
      if (pid === 0) {
        throw (`runCommand returned no pid. (Insufficient RAM, or bad command?) `
          +`Destination: ${fNameCommand} Command: ${commandToFile}`)
      }
      await waitForProcessToComplete_Custom(ns, fnIsAlive, pid, verbose)
      if (verbose) {
        ns.print(`Process ${pid} is done. Reading the contents of ${fName}...`)
      }

      // Read the output of the other script
      const fileData = ns.read(fName)
      if (fileData === undefined) {
        throw (`ns.read('${fName}') somehow returned undefined`)
      }
      if (fileData === "") {
        throw (`The expected output file ${fName} is empty.`)
      }
      if (verbose) {
        ns.print(`Read the following data for command ${command}:\n${fileData}`)
      }
      // Deserialize it back into an object/array and return
      return JSON.parse(fileData)
    }
    catch (error) {
      const errorLog = `getNsDataThroughFile error (${maxRetries} retries ` +
        `remaining): ${String(error)}`
      const type =  maxRetries > 0 ? 'warning' : 'error'
      announce(ns, errorLog, type)
      if (maxRetries <= 0) {
        throw error
      }
      await ns.sleep(retryDelayMs)
    }
  }
}

/** @param {NS} ns **/
export function checkNsInstance(ns: NS) {
  if (!ns.read)
    throw "The first argument to this function should be a 'ns' instance."
  return ns
}

/**
 * @param {NS} ns
 **/
export async function networkMap(ns: NS) {
  let map = getLSItem('NMAP')

  while ( map === undefined ) {
    ns.print(`map is undefined, running networkMapper.js`);
    // ns.run('networkMapper.js', 1);
    let mapper = new NetworkMapper(ns);
    await mapper.writeMap();
    await mySleep(200);
    map = getLSItem('NMAP');
  }
  return map;
}

/**
 * @param {NS} ns
 * @param {string} serverName
 **/
export async function fetchServer(ns: NS, serverName: string) {
  let map = await networkMap(ns)
  return map[serverName]
}

/**
 * @param {string} serverName
 **/
export async function fetchServerFree(serverName: string) {
  let map = await networkMapFree()
  return map[serverName]
}

/**
 * @param {NS} ns
 **/
export async function networkMapFree() {
  let map = getLSItem('NMAP');

  while ( map === undefined ) {
    await mySleep(50);
    map = getLSItem('NMAP');
  }

  return map;
}

/**
 * @param {NS} ns
 * @param {string} goal
 **/
export async function findPath(goal: string) {
  let nMap = await networkMapFree();

  let path = [];
  while (true) {
    path.unshift(goal);
    goal = nMap[goal].parent;
    if (goal == '') {
      return path;
    }
  }
}

/**
 * @param {NS} ns
 * @param {any} target
 **/
export function root (ns: NS, target: any) {
  let player = fetchPlayer();

  if (target.data.hasAdminRights) {
    ns.print("Have root access already");
    return;
  }

  if ( target.portsRequired > toolsCount() ) {
    ns.print("Not enough tools to nuke this server.");
    return;
  }

  if (player.programs.includes("BruteSSH.exe")) {
    ns.brutessh(target.name);
  }
  if (player.programs.includes("FTPCrack.exe")) {
    ns.ftpcrack(target.name);
  }
  if (player.programs.includes("HTTPWorm.exe")) {
    ns.httpworm(target.name);
  }
  if (player.programs.includes("relaySMTP.exe")) {
    ns.relaysmtp(target.name);
  }
  if (player.programs.includes("SQLInject.exe")) {
    ns.sqlinject(target.name);
  }

  let ret = ns.nuke(target.name);
  ns.print("Sudo aquired: " + ret);
}
