export declare const factionServers: {
    CSEC: string;
    "avmnite-02h": string;
    "I.I.I.I": string;
    run4theh111z: string;
    "The-Cave": string;
};
export declare const rootFiles: {
    name: string;
    cost: number;
}[];
export declare const purchaseables: {
    name: string;
    cost: number;
}[];
export declare const gangEquipment: {
    weapons: string[];
    armor: string[];
    vehicles: string[];
    rootkits: string[];
    hackAugs: string[];
    combatAugs: string[];
};
export declare const lsKeys: {
    NMAP: string;
    PLAYER: string;
    RESERVE: string;
    BITNODE: string;
    WORKING: string;
    DECOMMISSIONED: string;
    HACKPERCENT: string;
};
