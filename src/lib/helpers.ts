import {NS} from "../../index";
import { rootFiles, purchaseables,lsKeys } from "./constants.js"

/**
 * @param {number} ms: milliseconds to sleep
 * @cost 0 GB
 */
export function mySleep(ms: number){
  return new Promise(resolve => setTimeout(resolve, ms))
}

/**
 * @returns {number} number of exe rootfiles on the player's computer
 * @cost 0 GB
 */
export function toolsCount() {
  let player = getLSItem('player')
  return (rootFiles.filter((file: any) => player.programs.includes(file.name))).length
}

/**
 * @param {NS} ns
 * @param {array} listOfLogs: list of loggable functions to disable
 * @cost 0 GB
 */
export function disableLogs(ns: NS, listOfLogs: any[]) {
  ['disableLog'].concat(...listOfLogs).forEach(log => ns.disableLog(log));
}

/**
 * @param {NS} ns
 * @cost 0.1 GB
 * @returns {number} player's money available
 */
export function myMoney(ns: NS) {
  return ns.getServerMoneyAvailable('home')
}

/**
 * @param {NS} ns
 * @param {number} cost - amount wanted
 * @cost 0.2 GB
 */
export async function waitForCash(ns: NS, cost: number) {
  if ((myMoney(ns) - reserve(ns)) >= cost) {
    ns.print("I have enough: " + ns.nFormat(cost, "$0.000a"))
    return;
  }
  ns.print("Waiting for " + ns.nFormat(cost + reserve(ns), "$0.000a"))
  while ((myMoney(ns) - reserve(ns)) < cost) {
    await ns.sleep(3000)
  }
}

/**
 * Reserve a certain amount for big purchases
 * You can manually reserve an amount by setting a number in localStorage.
 *     run lsSet.js reserve 4.5e9
 *
 * @param {NS} ns
 * @cost 0.1 GB
 */
export function reserve(ns: NS) {
  let manualReserve = Number(getLSItem('reserve') || 0)
  for ( const file of purchaseables ) {
    if (!ns.fileExists(file.name, 'home')) {
      return file.cost + manualReserve
    }
  }
  return manualReserve
}


/**
 * @param {NS} ns
 * @param {function} callback
 * @cost 0 GB
 */
export async function tryRun(ns: NS, callback: Function) {
  let pid = callback()
  while (pid == 0) {
    await ns.sleep(30)
    pid = callback()
  }
  return pid
}

/**
 * @param {string} key
 * @return {any} The value read from localStorage
 * @cost 0 GB
 **/
export function getLSItem(key: string) {
  // @ts-ignore
  let item = localStorage.getItem(lsKeys[key.toUpperCase()])

  return item ? JSON.parse(item) : undefined
}

/**
 * @param {string} key
 * @param {any} value
 * @cost 0 GB
 **/
export function setLSItem(key: string, value: any) {
  // @ts-ignore
  localStorage.setItem(lsKeys[key.toUpperCase()], JSON.stringify(value))
}

/**
 * @param {string} key
 * @cost 0 GB
 **/
export function clearLSItem(key: string) {
  // @ts-ignore
  localStorage.removeItem(lsKeys[key.toUpperCase()])
}

/**
 * @return {object} The player data from localStorage
 * @cost 0 GB
 **/
export function fetchPlayer() {
  return getLSItem('player')
}


/**
 * @param {NS} ns
 * @param log
 * @param toastVariant
 * @cost 0 GB
 * Prints a message, and also toasts it!
 */
export function announce(ns: NS, log: string, toastVariant = 'info') {
  // If an error is caught because the script is killed, ns becomes undefined
  if (!ns.print || !ns.toast) return;
  ns.print(`${toastVariant.toUpperCase()}: ${log}`);
  ns.toast(log, toastVariant.toLowerCase());
}


// yoink: https://gist.github.com/robmathers/1830ce09695f759bf2c4df15c29dd22d
/**
 * @param {array} data is an array of objects
 * @param {string|function} key is the key, property accessor, or callback
 *                          function to group by
 **/
export function groupBy(data: any[], key: string | Function) {
  // reduce runs this anonymous function on each element of `data`
  // (the `item` parameter, returning the `storage` parameter at the end
  return data.reduce(function(storage, item) {
    // get the first instance of the key by which we're grouping
    let group = key instanceof Function ? key(item) : item[key];

    // set `storage` for this instance of group to the outer scope (if not
    // empty) or initialize it
    storage[group] = storage[group] || [];

    // add this item to its group within `storage`
    storage[group].push(item);

    // return the updated storage to the reduce function,
    //which will then loop through the next
    return storage;
  }, {}); // {} is the initial value of the storage
};


// All of the below functions are stolen & reformatted/refactored from Insight's
// helper. They are required to make his scripts work.

/**
 * Return a formatted representation of the monetary amount using scale sympols
 * (e.g. $6.50M)
 * @param {number} num - The number to format
 * @param {number=} maxSigFigures - (default: 6) The maximum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} maxDecimalPlaces - (default: 3) The maximum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export function formatMoney(num: number, maxSigFigures = 6, maxDecimalPlaces = 3) {
  let numberShort = formatNumberShort(num, maxSigFigures, maxDecimalPlaces)
  return num >= 0 ? "$" + numberShort : numberShort.replace("-", "-$")
}

/**
 * Return a formatted representation of the monetary amount using scale symbols
 * (e.g. 6.50M)
 * @param {number} num - The number to format
 * @param {number=} maxSigFigures - (default: 6) The maximum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} maxDecimalPlaces - (default: 3) The maximum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export function formatNumberShort(num: number, maxSigFigures = 6, maxDecimalPlaces = 3) {
  const symbols = ["", "k", "m", "b", "t", "qa", "qi", "sx", "sp", "oc", "e30",
    "e33", "e36", "e39"]
  const sign = Math.sign(num) < 0 ? "-" : ""
  let i = 0;
  for (num = Math.abs(num); num >= 1000 && i < symbols.length; i++) {
    num /= 1000
  }
  const sigFigs = maxSigFigures - Math.floor(1 + Math.log10(num))
  const fixed = num.toFixed(Math.max(0, Math.min(maxDecimalPlaces, sigFigs)))
  return sign + fixed + symbols[i]
}

/**
 * Return a number formatted with the specified number of significant figures
 * or decimal places, whichever is more limiting.
 * @param {number} num - The number to format
 * @param {number=} minSigFigures - (default: 6) The minimum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} minDecimalPlaces - (default: 3) The minimum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export function formatNumber(num: number, minSigFigures = 3, minDecimalPlaces = 1) {
  if ( num == 0.0 )
    return  num

  let sigFigs = Math.max(0, minSigFigures - Math.ceil(Math.log10(num)))
  return num.toFixed(Math.max(minDecimalPlaces, sigFigs))
}

/**
 * Formats some RAM amount as a round number of GB with thousands separators
 * e.g. `1,028 GB`
 * @param {number} num - the number to format
 */
export function formatRam(num: number) {
  return `${Math.round(num).toLocaleString()} GB`;
}

/**
 * Format a duration (in milliseconds) as e.g. '1h 21m 6s' for big durations or
 * e.g '12.5s' / '23ms' for small durations
 * @param {number} duration - duration in milliseconds
 **/
export function formatDuration(duration: number) {
  if (duration < 1000) return `${duration.toFixed(0)}ms`
  const portions = [];
  const msInHour = 1000 * 60 * 60;
  const hours = Math.trunc(duration / msInHour);
  if (hours > 0) {
    portions.push(hours + 'h');
    duration -= (hours * msInHour);
  }
  const msInMinute = 1000 * 60;
  const minutes = Math.trunc(duration / msInMinute);
  if (minutes > 0) {
    portions.push(minutes + 'm');
    duration -= (minutes * msInMinute);
  }
  let seconds: number = (duration / 1000.0)
  // Include millisecond precision if we're on the order of seconds
  seconds = parseFloat((hours == 0 && minutes == 0) ? seconds.toPrecision(3) : seconds.toFixed(0));
  if (seconds > 0) {
    portions.push(seconds + 's');
    duration -= (minutes * 1000);
  }
  return portions.join(' ');
}

/**
 * Generate a hashCode for a string that is pretty unique most of the time
 * @param {string} s: String to generate hashcode for
 **/
export function hashCode(s: string) {
  return s.split("").reduce(function (a, b) {
    a = ((a << 5) - a) + b.charCodeAt(0)
    return a & a
  }, 0)
}

// FUNCTIONS THAT PROVIDE ALTERNATIVE IMPLEMENTATIONS TO EXPENSIVE NS FUNCTIONS
// VARIATIONS ON NS.RUN

/**
 * @param {NS} ns
 *  Use where a function is required to run a script and you have already
 * referenced ns.run in your script
 **/
export function getFnRunViaNsRun(ns: NS) { return checkNsInstance(ns).run; }

/**
 * @param {NS} ns
 * @param {string} host
 * Use where a function is required to run a script and you have already
 * referenced ns.exec in your script
 **/
export function getFnRunViaNsExec(ns: NS, host = "home") {
  checkNsInstance(ns);
  return function (scriptPath: any, ...args: any) {
    return ns.exec(scriptPath, host, ...args)
  }
}
// VARIATIONS ON NS.ISRUNNING

/**
 * @param {NS} ns
 * Use where a function is required to check if a script is running and you have
 * already referenced ns.isRunning in your script
 **/
export function getFnIsAliveViaNsIsRunning(ns: NS) {
  return checkNsInstance(ns).isRunning
}

/**
 * @param {NS} ns
 * Use where a function is required to check if a script is running and you have
 * already referenced ns.ps in your script
 **/
export function getFnIsAliveViaNsPs(ns: NS) {
  checkNsInstance(ns);
  return function (pid: number, host: string) {
    return ns.ps(host).some(process => process.pid === pid)
  }
}

/**
 * Evaluate an arbitrary ns command by writing it to a new script and then
 * running or executing the new file
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                  run as a new script.
 */
export async function runCommand(ns: NS, command: string, fileName: string, verbose: boolean, ...args: any) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['run', 'sleep'])
  return await runCommand_Custom(ns, ns.run, command,fileName, verbose, ...args)
}

/**
 * Evaluate an arbitrary ns command by writing it to a new script, running the
 * script, then waiting for it to complete running.
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                           desired result (e.g. "ns.exec('nuker.js', 'home')")
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                             of the file to which data will be written to disk
 *                             by a temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                          result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                         run as a new script
 */
export async function runCommandAndWait(ns: NS, command: string, fileName: string, verbose: boolean, ...args: any) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['run', 'sleep'])

  const pid = await runCommand_Custom(ns,ns.run,command,fileName,verbose,...args)
  if (pid === 0) {
    throw (`runCommand returned no pid. (Insufficient RAM, or bad command?) ` +
      `Destination: ${fileName} Command: ${command}`)
  }
  await waitForProcessToComplete_Custom(ns, ns.isRunning, pid, verbose)
}

/**
 * An advanced version of runCommand that lets you pass your own "isAlive" test
 * to reduce RAM requirements (e.g. to avoid referencing ns.isRunning)
 *
 * Importing incurs 0 GB RAM (assuming fnRun, fnWrite are implemented using
 * another ns function you already reference elsewhere like ns.exec)
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {function} fnRun - A single-argument function used to start the new
 *                   script, e.g. `ns.run` or
 *                   `(f,...args) => ns.exec(f, "home", ...args)`
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                  run as a new script.
 **/
export async function runCommand_Custom(ns: NS, fnRun: Function, command: string, fileName: string, verbose: boolean, ...args: any) {
  checkNsInstance(ns)
  const helpers = [
    'mySleep', 'toolsCount', 'myMoney', 'waitForCash', 'reserve',
    'tryRun', 'getLSItem', 'setLSItem', 'clearLSItem', 'fetchPlayer',
    'announce', 'groupBy', 'formatMoney', 'formatNumberShort', 'formatNumber',
    'formatDuration', 'formatRam', 'hashCode',
  ]
  const script =
    // `import { ${helpers.join(', ')} } fr` + `om 'helpers.js';\n` +
    // `import { networkMap, fetchServer } fr` + `om 'network.js';\n` +
    // `import * as constants fr` + `om 'constants.js';\n` +
    `export async function main(ns) { try { ` +
    (verbose ? `let output = ${command}; ns.tprint(output)` : command) +
    `; } catch(err) { ns.tprint(String(err)); throw(err); } }`;
  fileName = fileName || `/Temp/${hashCode(command)}-command.js`;
  // To improve performance and save on garbage collection, we can skip
  // writing this exact same script was previously written (common for
  // repeatedly-queried data)
  if (ns.read(fileName) != script) {
    await ns.write(fileName, script as any, "w")
  }
  return fnRun(fileName, ...args)
}

/**
 * Wait for a process id to complete running
 * Importing incurs a maximum of 0.1 GB RAM (for ns.isRunning)
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {int} pid - The process id to monitor
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 **/
export async function waitForProcessToComplete(ns: NS, pid: number, verbose: boolean) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['isRunning'])
  return await waitForProcessToComplete_Custom(ns, ns.isRunning, pid, verbose)
}
/**
 * An advanced version of waitForProcessToComplete that lets you pass your own
 * "isAlive" test to reduce RAM requirements (e.g. to avoid referencing
 * ns.isRunning)
 *
 * Importing incurs 0 GB RAM (assuming fnIsAlive is implemented using another ns
 * function you already reference elsewhere like ns.ps)
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {function} fnIsAlive - A single-argument function used to test, e.g.
 *                   `ns.isRunning` or
 *                   `pid => ns.ps("home").some(process => process.pid === pid)`
 * @param {string | number} pid - Process PID
 * @param {boolean} verbose - Verbose log output
 **/
export async function waitForProcessToComplete_Custom(ns: NS, fnIsAlive: Function, pid: string | number, verbose: boolean) {
  checkNsInstance(ns);
  if (!verbose) disableLogs(ns, ['sleep']);
  // Wait for the PID to stop running (cheaper than e.g. deleting (rm) a
  // possibly pre-existing file and waiting for it to be recreated)
  for (var retries = 0; retries < 1000; retries++) {
    if (!fnIsAlive(pid)) break; // Script is done running
    if (verbose && retries % 100 === 0) {
      ns.print(`Waiting for pid ${pid} to complete... (${retries})`)
    }
    await ns.sleep(10);
  }
  // Make sure that the process has shut down and we haven't just stopped retrying
  if (fnIsAlive(pid)) {
    let error = `run-command pid ${pid} is running much longer than expected. `+
      `Max retries exceeded.`
    ns.print(error)
    throw error
  }
}

/**
 * Retrieve the result of an ns command by executing it in a temporary .js
 * script, writing the result to a file, then shutting it down
 *
 * Importing incurs a maximum of 1.1 GB RAM (0 GB for ns.read, 1 GB for ns.run,
 * 0.1 GB for ns.isRunning).
 *
 * Has the capacity to retry if there is a failure (e.g. due to lack of RAM
 * available). Not recommended for performance-critical code.
 *
 * @param {NS} ns - The netscript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 * @param {integer} maxRetries - (default 5) How many times to retry for not
 *                  more RAM before throwing an error
 * @param {integer} retryDelayMs - (default 50) How many milliseconds to wait
 *                  before retrying
 **/
export async function getNsDataThroughFile(ns: NS, command: string, fName: string, verbose: boolean, maxRetries = 5, retryDelayMs = 50) {
  checkNsInstance(ns)
  if (!verbose) disableLogs(ns, ['run', 'isRunning'])
  return await getNsDataThroughFile_Custom(ns,
    ns.run,
    ns.isRunning,
    command,
    fName,
    verbose,
    maxRetries,
    retryDelayMs)
}
/**
 * An advanced version of getNsDataThroughFile that lets you pass your own
 * "fnRun" and "fnIsAlive" implementations to reduce RAM requirements
 *
 * Importing incurs no RAM (now that ns.read is free) plus whatever fnRun /
 * fnIsAlive you provide it
 *
 * Has the capacity to retry if there is a failure (e.g. due to lack of RAM
 * available). Not recommended for performance-critical code.
 *
 * @param {NS} ns - The netscript instance passed to your script
 * @param {function} fnRun - A single-argument function used to start the new
 *                   script, e.g. `ns.run` or
 *                   `(f,...args) => ns.exec(f, "home", ...args)`
 * @param {function} fnIsAlive - A single-argument function used to test if the
 *                   script has completed, e.g. `ns.isRunning` or
 *                   `pid => ns.ps("home").some(process => process.pid === pid)`
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 * @param {integer} maxRetries - (default 5) How many times to retry for not
 *                  more RAM before throwing an error
 * @param {integer} retryDelayMs - (default 50) How many milliseconds to wait
 *                  before retrying
 **/
export async function getNsDataThroughFile_Custom(ns: NS, fnRun: Function, fnIsAlive: Function, command: string, fName: string, verbose: boolean, maxRetries = 5, retryDelayMs = 50) {
  checkNsInstance(ns);
  if (!verbose) disableLogs(ns, ['read'])
  const commandHash = hashCode(command)
  fName = fName || `/Temp/${commandHash}-data.txt`
  const fNameCommand = (fName || `/Temp/${commandHash}-command`) + '.js'
  // Prepare a command that will write out a new file containing the results of
  // the command unless it already exists with the same contents
  // (saves time/ram to check first)
  const commandToFile = `const result = JSON.stringify(${command}); ` +
    `if (ns.read("${fName}") != result) await ns.write("${fName}", result, 'w')`
  while (maxRetries-- > 0) {
    try {
      const pid = await runCommand_Custom(ns, fnRun, commandToFile, fNameCommand, false)
      if (pid === 0) {
        throw (`runCommand returned no pid. (Insufficient RAM, or bad command?) `
          +`Destination: ${fNameCommand} Command: ${commandToFile}`)
      }
      await waitForProcessToComplete_Custom(ns, fnIsAlive, pid, verbose)
      if (verbose) {
        ns.print(`Process ${pid} is done. Reading the contents of ${fName}...`)
      }

      // Read the output of the other script
      const fileData = ns.read(fName)
      if (fileData === undefined) {
        throw (`ns.read('${fName}') somehow returned undefined`)
      }
      if (fileData === "") {
        throw (`The expected output file ${fName} is empty.`)
      }
      if (verbose) {
        ns.print(`Read the following data for command ${command}:\n${fileData}`)
      }
      // Deserialize it back into an object/array and return
      return JSON.parse(fileData)
    }
    catch (error) {
      const errorLog = `getNsDataThroughFile error (${maxRetries} retries ` +
        `remaining): ${String(error)}`
      const type =  maxRetries > 0 ? 'warning' : 'error'
      announce(ns, errorLog, type)
      if (maxRetries <= 0) {
        throw error
      }
      await ns.sleep(retryDelayMs)
    }
  }
}

/** @param {NS} ns **/
export function checkNsInstance(ns: NS) {
  if (!ns.read)
    throw "The first argument to this function should be a 'ns' instance."
  return ns
}
