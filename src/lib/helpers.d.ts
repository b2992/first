import { NS } from "../../index";
/**
 * @param {number} ms: milliseconds to sleep
 * @cost 0 GB
 */
export declare function mySleep(ms: number): Promise<unknown>;
/**
 * @returns {number} number of exe rootfiles on the player's computer
 * @cost 0 GB
 */
export declare function toolsCount(): number;
/**
 * @param {NS} ns
 * @param {array} listOfLogs: list of loggable functions to disable
 * @cost 0 GB
 */
export declare function disableLogs(ns: NS, listOfLogs: any[]): void;
/**
 * @param {NS} ns
 * @cost 0.1 GB
 * @returns {number} player's money available
 */
export declare function myMoney(ns: NS): number;
/**
 * @param {NS} ns
 * @param {number} cost - amount wanted
 * @cost 0.2 GB
 */
export declare function waitForCash(ns: NS, cost: number): Promise<void>;
/**
 * Reserve a certain amount for big purchases
 * You can manually reserve an amount by setting a number in localStorage.
 *     run lsSet.js reserve 4.5e9
 *
 * @param {NS} ns
 * @cost 0.1 GB
 */
export declare function reserve(ns: NS): number;
/**
 * @param {NS} ns
 * @param {function} callback
 * @cost 0 GB
 */
export declare function tryRun(ns: NS, callback: Function): Promise<any>;
/**
 * @param {string} key
 * @return {any} The value read from localStorage
 * @cost 0 GB
 **/
export declare function getLSItem(key: string): any;
/**
 * @param {string} key
 * @param {any} value
 * @cost 0 GB
 **/
export declare function setLSItem(key: string, value: any): void;
/**
 * @param {string} key
 * @cost 0 GB
 **/
export declare function clearLSItem(key: string): void;
/**
 * @return {object} The player data from localStorage
 * @cost 0 GB
 **/
export declare function fetchPlayer(): any;
/**
 * @param {NS} ns
 * @param log
 * @param toastVariant
 * @cost 0 GB
 * Prints a message, and also toasts it!
 */
export declare function announce(ns: NS, log: string, toastVariant?: string): void;
/**
 * @param {array} data is an array of objects
 * @param {string|function} key is the key, property accessor, or callback
 *                          function to group by
 **/
export declare function groupBy(data: any[], key: string | Function): any;
/**
 * Return a formatted representation of the monetary amount using scale sympols
 * (e.g. $6.50M)
 * @param {number} num - The number to format
 * @param {number=} maxSigFigures - (default: 6) The maximum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} maxDecimalPlaces - (default: 3) The maximum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export declare function formatMoney(num: number, maxSigFigures?: number, maxDecimalPlaces?: number): string;
/**
 * Return a formatted representation of the monetary amount using scale symbols
 * (e.g. 6.50M)
 * @param {number} num - The number to format
 * @param {number=} maxSigFigures - (default: 6) The maximum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} maxDecimalPlaces - (default: 3) The maximum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export declare function formatNumberShort(num: number, maxSigFigures?: number, maxDecimalPlaces?: number): string;
/**
 * Return a number formatted with the specified number of significant figures
 * or decimal places, whichever is more limiting.
 * @param {number} num - The number to format
 * @param {number=} minSigFigures - (default: 6) The minimum significant figures
 *                  you wish to see (e.g. 123, 12.3 and 1.23 all have 3
 *                  significant figures)
 * @param {number=} minDecimalPlaces - (default: 3) The minimum decimal places
 *                  you wish to see, regardless of significant figures. (e.g.
 *                  12.3, 1.2, 0.1 all have 1 decimal)
 **/
export declare function formatNumber(num: number, minSigFigures?: number, minDecimalPlaces?: number): string | 0;
/**
 * Formats some RAM amount as a round number of GB with thousands separators
 * e.g. `1,028 GB`
 * @param {number} num - the number to format
 */
export declare function formatRam(num: number): string;
/**
 * Format a duration (in milliseconds) as e.g. '1h 21m 6s' for big durations or
 * e.g '12.5s' / '23ms' for small durations
 * @param {number} duration - duration in milliseconds
 **/
export declare function formatDuration(duration: number): string;
/**
 * Generate a hashCode for a string that is pretty unique most of the time
 * @param {string} s: String to generate hashcode for
 **/
export declare function hashCode(s: string): number;
/**
 * @param {NS} ns
 *  Use where a function is required to run a script and you have already
 * referenced ns.run in your script
 **/
export declare function getFnRunViaNsRun(ns: NS): (script: string, numThreads?: number | undefined, ...args: string[]) => number;
/**
 * @param {NS} ns
 * @param {string} host
 * Use where a function is required to run a script and you have already
 * referenced ns.exec in your script
 **/
export declare function getFnRunViaNsExec(ns: NS, host?: string): (scriptPath: any, ...args: any) => number;
/**
 * @param {NS} ns
 * Use where a function is required to check if a script is running and you have
 * already referenced ns.isRunning in your script
 **/
export declare function getFnIsAliveViaNsIsRunning(ns: NS): (script: string, host: string, ...args: string[]) => boolean;
/**
 * @param {NS} ns
 * Use where a function is required to check if a script is running and you have
 * already referenced ns.ps in your script
 **/
export declare function getFnIsAliveViaNsPs(ns: NS): (pid: number, host: string) => boolean;
/**
 * Evaluate an arbitrary ns command by writing it to a new script and then
 * running or executing the new file
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                  run as a new script.
 */
export declare function runCommand(ns: NS, command: string, fileName: string, verbose: boolean, ...args: any): Promise<any>;
/**
 * Evaluate an arbitrary ns command by writing it to a new script, running the
 * script, then waiting for it to complete running.
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                           desired result (e.g. "ns.exec('nuker.js', 'home')")
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                             of the file to which data will be written to disk
 *                             by a temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                          result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                         run as a new script
 */
export declare function runCommandAndWait(ns: NS, command: string, fileName: string, verbose: boolean, ...args: any): Promise<void>;
/**
 * An advanced version of runCommand that lets you pass your own "isAlive" test
 * to reduce RAM requirements (e.g. to avoid referencing ns.isRunning)
 *
 * Importing incurs 0 GB RAM (assuming fnRun, fnWrite are implemented using
 * another ns function you already reference elsewhere like ns.exec)
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {function} fnRun - A single-argument function used to start the new
 *                   script, e.g. `ns.run` or
 *                   `(f,...args) => ns.exec(f, "home", ...args)`
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fileName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, the evaluation
 *                result of the command is printed to the terminal
 * @param {...args} args - args to be passed in as arguments to command being
 *                  run as a new script.
 **/
export declare function runCommand_Custom(ns: NS, fnRun: Function, command: string, fileName: string, verbose: boolean, ...args: any): Promise<any>;
/**
 * Wait for a process id to complete running
 * Importing incurs a maximum of 0.1 GB RAM (for ns.isRunning)
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {int} pid - The process id to monitor
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 **/
export declare function waitForProcessToComplete(ns: NS, pid: number, verbose: boolean): Promise<void>;
/**
 * An advanced version of waitForProcessToComplete that lets you pass your own
 * "isAlive" test to reduce RAM requirements (e.g. to avoid referencing
 * ns.isRunning)
 *
 * Importing incurs 0 GB RAM (assuming fnIsAlive is implemented using another ns
 * function you already reference elsewhere like ns.ps)
 *
 * @param {NS} ns - The nestcript instance passed to your script
 * @param {function} fnIsAlive - A single-argument function used to test, e.g.
 *                   `ns.isRunning` or
 *                   `pid => ns.ps("home").some(process => process.pid === pid)`
 * @param {string | number} pid - Process PID
 * @param {boolean} verbose - Verbose log output
 **/
export declare function waitForProcessToComplete_Custom(ns: NS, fnIsAlive: Function, pid: string | number, verbose: boolean): Promise<void>;
/**
 * Retrieve the result of an ns command by executing it in a temporary .js
 * script, writing the result to a file, then shutting it down
 *
 * Importing incurs a maximum of 1.1 GB RAM (0 GB for ns.read, 1 GB for ns.run,
 * 0.1 GB for ns.isRunning).
 *
 * Has the capacity to retry if there is a failure (e.g. due to lack of RAM
 * available). Not recommended for performance-critical code.
 *
 * @param {NS} ns - The netscript instance passed to your script
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 * @param {integer} maxRetries - (default 5) How many times to retry for not
 *                  more RAM before throwing an error
 * @param {integer} retryDelayMs - (default 50) How many milliseconds to wait
 *                  before retrying
 **/
export declare function getNsDataThroughFile(ns: NS, command: string, fName: string, verbose: boolean, maxRetries?: number, retryDelayMs?: number): Promise<any>;
/**
 * An advanced version of getNsDataThroughFile that lets you pass your own
 * "fnRun" and "fnIsAlive" implementations to reduce RAM requirements
 *
 * Importing incurs no RAM (now that ns.read is free) plus whatever fnRun /
 * fnIsAlive you provide it
 *
 * Has the capacity to retry if there is a failure (e.g. due to lack of RAM
 * available). Not recommended for performance-critical code.
 *
 * @param {NS} ns - The netscript instance passed to your script
 * @param {function} fnRun - A single-argument function used to start the new
 *                   script, e.g. `ns.run` or
 *                   `(f,...args) => ns.exec(f, "home", ...args)`
 * @param {function} fnIsAlive - A single-argument function used to test if the
 *                   script has completed, e.g. `ns.isRunning` or
 *                   `pid => ns.ps("home").some(process => process.pid === pid)`
 * @param {string} command - The ns command that should be invoked to get the
 *                 desired data (e.g. "ns.getServer('home')" )
 * @param {string=} fName - (default "/Temp/{commandhash}-data.txt") The name
 *                  of the file to which data will be written to disk by a
 *                  temporary process
 * @param {bool=} verbose - (default false) If set to true, pid and result of
 *                command are logged.
 * @param {integer} maxRetries - (default 5) How many times to retry for not
 *                  more RAM before throwing an error
 * @param {integer} retryDelayMs - (default 50) How many milliseconds to wait
 *                  before retrying
 **/
export declare function getNsDataThroughFile_Custom(ns: NS, fnRun: Function, fnIsAlive: Function, command: string, fName: string, verbose: boolean, maxRetries?: number, retryDelayMs?: number): Promise<any>;
/** @param {NS} ns **/
export declare function checkNsInstance(ns: NS): NS;
