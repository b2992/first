import { CachedData, tryRun } from "common.js";
class CronServer {
    constructor(ns) {
        this.ns = ns;
        this.cronData = new CachedData(this.ns, 'cronJobs', {
            localStorage: true,
            localStorageFrequency: 20,
            file: '/data/CronJobs',
            fileFrequency: 5 * 1000
        });
        this.jobs = [];
        this.running = true;
        ns.disableLog('sleep');
    }
    async init() {
        await this.cronData.sync();
        this.jobs = this.cronData.data;
        while (this.running) {
            await this.executeJobs();
            await this.ns.sleep(5);
        }
    }
    async executeJobs() {
        let me = this;
        let first = true;
        let proc = null;
        for (let job of me.jobs) {
            proc = me.ns.ps('home').find(p => p.filename == job.file);
            if (!proc && Date.now() > job.last + job.frequency) {
                await tryRun(me.ns, () => me.ns.run(job.file, 1));
                job.last = Date.now();
                await me.cronData.updateData(me.jobs);
            }
            if (first) {
                await me.ns.sleep(50);
                first = false;
            }
        }
        await me.ns.sleep(5);
    }
}
const commands = {
    init: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            let cronData = new CachedData(ns, 'cronJobs', {
                data: [],
                localStorage: true,
                localStorageFrequency: 20,
                file: '/data/CronJobs',
                fileFrequency: 5 * 1000
            });
            // await cronData.save();
            ns.tprint(`INFO Cron Server Initialized!`);
        }
    },
    start: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            let cronServer = new CronServer(ns);
            await cronServer.init();
        }
    },
    stop: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            let killed = ns.kill('/services/cron.js', 'home', 'start');
            ns.tprint(`Cron Service Stopped: ${killed}`);
        }
    },
    list: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            let cronData = new CachedData(ns, 'cronJobs', {
                localStorage: true,
                localStorageFrequency: 20,
                file: '/data/CronJobs',
                fileFrequency: 5 * 1000
            });
            await cronData.sync();
            let jobs = cronData.data;
            for (let j in jobs) {
                let job = jobs[j];
                ns.tprint(`INFO Job(${j}) Frequency(${job.frequency}) LastUpdate(${job.last}) File(${job.file})`);
            }
        }
    },
    add: {
        args: (data, args) => {
            data.flags([
                ['file', 'file.js'],
                ['frequency', 100],
            ]);
            let lastArg = args[args.length - 1];
            if (lastArg === '--file') {
                return [...data.scripts];
            }
            else {
                return [];
            }
        },
        handler: async (ns) => {
            let args = ns.flags([
                ['file', 'file.js'],
                ['frequency', 100],
            ]);
            let cronData = new CachedData(ns, 'cronJobs', {
                localStorage: true,
                localStorageFrequency: 20,
                file: '/data/CronJobs',
                fileFrequency: 5 * 1000
            });
            cronData.load();
            let jobs = cronData.data;
            jobs.push({ file: args.file, frequency: args.frequency, last: 0 });
            cronData.data = jobs;
            await cronData.save();
        }
    },
    del: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            let cronData = new CachedData(ns, 'cronJobs', {
                localStorage: true,
                localStorageFrequency: 20,
                file: '/data/CronJobs',
                fileFrequency: 5 * 1000
            });
            cronData.load();
            let jobs = cronData.data;
            jobs.splice(ns.args[1], 1);
            cronData.data = jobs;
            await cronData.save();
        }
    },
};
export function autocomplete(data, args) {
    if (commands[args[0]]) {
        return commands[args[0]].args(data, args);
    }
    else if (args.length === 0 || args.length === 1) {
        return Object.keys(commands);
    }
    else {
        return [];
    }
}
export async function main(ns) {
    let command = commands[ns.args[0]];
    if (command) {
        await command.handler(ns);
    }
}
