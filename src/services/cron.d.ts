import { NS } from "../../index";
export declare function autocomplete(data: {
    servers: any;
    txts: any;
    scripts: any;
    flags: Function;
}, args: string[]): any;
export declare function main(ns: NS): Promise<void>;
