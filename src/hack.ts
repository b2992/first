import {NS} from "../index";

// let target = 'n00dles';
// let target = 'joesguns';
// let target = 'foodnstuff';
// let target = 'iron-gym';
let target = 'omega-net';

/** @param {NS} ns **/
export async function main(ns: NS) {
  let portsOpened = 0;
  let portsRequired = ns.getServerNumPortsRequired(target);
  if (portsRequired > portsOpened && ns.fileExists("BruteSSH.exe", "home")) {
    ns.brutessh(target);
    portsOpened++;
  }
  if (portsRequired > portsOpened && ns.fileExists("FTPCrack.exe", "home")) {
    ns.ftpcrack(target);
    portsOpened++;
  }
  if (portsRequired > portsOpened && ns.fileExists("relaySMTP.exe", "home")) {
    ns.relaysmtp(target);
    portsOpened++;
  }
  if (portsRequired > portsOpened && ns.fileExists("HTTPWorm.exe", "home")) {
    ns.httpworm(target);
    portsOpened++;
  }
  if (portsRequired > portsOpened && ns.fileExists("SQLInject.exe", "home")) {
    ns.sqlinject(target);
    portsOpened++;
  }
  if (portsOpened >= portsRequired) {
    if (portsOpened > 0) {
      ns.tprint(`Server(${target}): Opened ${portsOpened}/${portsRequired} ports`);
    }
    ns.tprint(`Server(${target}): Rooting`);
    ns.nuke(target);
    ns.tprint(`Server(${target}): Rooted`);
    while (true) {
      let moneyThresh = ns.getServerMaxMoney(target) * 0.75;
      let securityThresh = ns.getServerMinSecurityLevel(target) + 10;
      let securityLevel = ns.getServerSecurityLevel(target);
      let moneyAvailable = ns.getServerMoneyAvailable(target);
      if (securityLevel > securityThresh) {
        await ns.weaken(target);
      } else if (moneyAvailable < moneyThresh) {
        await ns.grow(target);
      } else {
        await ns.hack(target);
      }
    }
  }

}
