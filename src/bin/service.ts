import {NS} from "../../index";

const commands = {
  start: {
    args: (data: {servers: any, txts: any[], scripts: any[], flags: Function}, args: string[]) => {
      return data.scripts
        .filter((script) => { return script.indexOf('/services') !== -1; })
        .map((script) => { return script.replace('/services/', '').replace('.js', ''); });
    },
    handler: async (ns: NS) => {
      let server = ns.getServer();
      let script = `/services/${ns.args[1]}.js`;
      if (ns.fileExists(script)) {
        if (!ns.isRunning(script, server.hostname, 'start')) {
          ns.run(script, 1, 'start');
        }
      }
      // let cronServer = new CronServer(ns);
      // await cronServer.init();
    }
  },
  stop: {
    args: (data: {servers: any, txts: any[], scripts: any[], flags: Function}, args: string[]) => {
      return data.scripts
        .filter((script) => { return script.indexOf('/services') !== -1; })
        .map((script) => { return script.replace('/services/', '').replace('.js', ''); });
    },
    handler: async (ns: NS) => {
      let server = ns.getServer();
      let script = `/services/${ns.args[1]}.js`;
      if (ns.fileExists(script)) {
        if (ns.isRunning(script, server.hostname, 'start')) {
          ns.run(script, 1, 'stop');
        }
      }
      // let cronServer = new CronServer(ns);
      // await cronServer.init();
    }
  },
  list: {
    args: (data: {servers: any, txts: any, scripts: any, flags: Function}, args: string[]) => {
      return [];
    },
    handler: async (ns: NS) => {
      let server = ns.getServer();
      let services = ns.ls(server.hostname, "/services");
      services.forEach((service) => {
        let serviceName = service.replace('/services/', '').replace('.js', '');
        let isRunning = ns.isRunning(service, server.hostname, 'start')
        ns.tprint(`INFO: Service(${serviceName}) Running(${isRunning})`);
      });
      // let cronServer = new CronServer(ns);
      // await cronServer.init();
    }
  },
}

export function autocomplete(data: {servers: any, txts: any, scripts: any, flags: Function}, args: string[]) {
  if ((commands as any)[args[0]]) {
    return (commands as any)[args[0]].args(data, args);
  }
  else if (args.length === 0 || args.length === 1) {
    return Object.keys(commands);
  }
  else {
    return [];
  }
}

export async function main(ns: NS) {
  let command = (commands as any)[ns.args[0] as string];
  if (command) {
    await command.handler(ns);
  }
}
