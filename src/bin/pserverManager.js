import { announce, clearLSItem, fetchServer, formatRam, networkMapFree, setLSItem, waitForCash } from "common.js";
const min = 60;
const hour = min * 50;
const commands = {
    info: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            const nMap = await networkMapFree();
            const pServers = Object.values(nMap).filter((s) => { return s.name !== 'home' && s.data.purchasedByPlayer; });
            const curRam = smallestSize(pServers);
            const nextRam = nextRamSize(ns, curRam);
            const limit = ns.getPurchasedServerLimit();
            const singleCost = ns.getPurchasedServerCost(2 ** nextRam);
            ns.tprint(`INFO: Purchased Servers Current Ram ${formatRam(curRam)} to ${formatRam(2 ** nextRam)}`);
            ns.tprint(`INFO: Next Upgrade costs ${ns.nFormat(singleCost, '$0.000a')} each and total ${ns.nFormat(singleCost * limit, '$0.000a')}`);
        }
    },
    upgrade: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            let hostname;
            const nMap = await networkMapFree();
            const pServers = Object.values(nMap).filter((s) => { return s.name !== 'home' && s.data.purchasedByPlayer; });
            const curRam = smallestSize(pServers);
            const nextRam = nextRamSize(ns, curRam);
            const limit = ns.getPurchasedServerLimit();
            const singleCost = ns.getPurchasedServerCost(2 ** nextRam);
            ns.tprint(`INFO: Purchased Servers Current Ram ${formatRam(curRam)} to ${formatRam(2 ** nextRam)}`);
            ns.tprint(`INFO: Next Upgrade costs ${ns.nFormat(singleCost, '$0.000a')} each and total ${ns.nFormat(singleCost * limit, '$0.000a')}`);
            let count = 0;
            for (let i = 0; i < limit; i++) {
                hostname = `pserv-${i}`;
                count += await buyNewOrReplaceServer(ns, hostname, singleCost, nextRam);
                await ns.sleep(2000);
            }
            let msg = `PServer Upgrade completed`;
            announce(ns, msg, 'success');
            ns.tprint(`Success: ${msg}`);
        }
    },
};
export function autocomplete(data, args) {
    if (commands[args[0]]) {
        return commands[args[0]].args(data, args);
    }
    else if (args.length === 0 || args.length === 1) {
        return Object.keys(commands);
    }
    else {
        return [];
    }
}
export async function main(ns) {
    let command = commands[ns.args[0]];
    if (command) {
        await command.handler(ns);
    }
}
function smallestSize(pServers) {
    if (pServers.length === 0) {
        return 0;
    }
    else {
        return pServers.reduce((prev, cur) => { return prev.maxRam < cur.maxRam ? prev : cur; }).maxRam;
    }
}
function nextRamSize(ns, curRam) {
    const limit = ns.getPurchasedServerLimit();
    const totIncomePerSec = ns.getScriptIncome()[0];
    const maxServerSize = ns.getPurchasedServerMaxRam();
    const incomePerPayoffTime = totIncomePerSec * 2 * hour;
    ns.print(`Total income: ${ns.nFormat(totIncomePerSec, "$0,0")}`);
    ns.print(`Income per payoff time: ${ns.nFormat(incomePerPayoffTime, "$0,0")}`);
    if (incomePerPayoffTime == 0)
        return 0;
    let cost, totalCost;
    for (let i = 20; 2 ** i > curRam; i--) {
        // max server size can vary based on BN
        if (2 ** i > maxServerSize)
            continue;
        if (i < 0) {
            ns.tail();
            throw `How is i less than 0? ${i}`;
        }
        cost = ns.getPurchasedServerCost(2 ** i);
        totalCost = cost * limit;
        ns.print(`Total cost for ${2 ** i}GB ram: ${ns.nFormat(totalCost, "$0,0")}`);
        if (totalCost < incomePerPayoffTime) {
            ns.print(`(${2 ** i}) totalCost < incomePerPayoffTime`);
            ns.print(`Returning ${2 ** i}`);
            return i;
        }
    }
    return 0;
}
/**
 * @param {NS} ns
 * @param {string} hostname
 * @param {number} cost
 * @param {number} ram
 */
async function buyNewOrReplaceServer(ns, hostname, cost, ram) {
    let host = await fetchServer(ns, hostname);
    if (host === null || host === undefined) {
        ns.print(`Buying a new server ${hostname} with ${ram} GB ram for ` +
            `${ns.nFormat(cost, "$0.000a")}`);
        return purchaseNewServer(ns, hostname, cost, ram);
    }
    if (host.maxRam >= ram) {
        ns.print(`${hostname} is large enough, with ${host.maxRam} GB ram`);
        return 0;
    }
    ns.print(`Upgrading ${hostname} with ${host.maxRam} -> ${ram} GB ram` +
        ` for ${ns.nFormat(cost, "$0.000a")}`);
    return await upgradeServer(ns, host, cost, ram);
}
/**
 * @param {NS} ns
 * @param {string} hostname
 * @param {number} cost
 * @param {number} ram
 */
async function purchaseNewServer(ns, hostname, cost, ram) {
    await waitForCash(ns, cost);
    let result = ns.purchaseServer(hostname, ram);
    if (result) {
        announce(ns, `Purchased new server, ${hostname} with ${formatRam(ram)}`);
        clearLSItem('nmap');
        return 1;
    }
    return 0;
}
/**
 * @param {NS} ns
 * @param {object} host
 * @param {number} cost
 * @param {number} ram
 */
async function upgradeServer(ns, host, cost, ram) {
    setLSItem('decommissioned', host.name);
    await waitForCash(ns, cost);
    ns.print("Waiting for scripts to end on " + host.name);
    await wrapUpProcesses(ns, host.name);
    await ns.sleep(50);
    ns.print("Destroying server: " + host.name);
    ns.deleteServer(host.name);
    const result = ns.purchaseServer(host.name, ram);
    clearLSItem('decommissioned');
    if (result) {
        announce(ns, `Upgraded server ${host.name} with ${formatRam(ram)}`);
        clearLSItem('nmap');
        return 1;
    }
    return 0;
}
/**
 * @param {NS} ns
 * @param {string} hostname
 */
async function wrapUpProcesses(ns, hostname) {
    while (ns.ps(hostname).length > 0) {
        await ns.sleep(200);
    }
}
