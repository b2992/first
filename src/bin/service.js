const commands = {
    start: {
        args: (data, args) => {
            return data.scripts
                .filter((script) => { return script.indexOf('/services') !== -1; })
                .map((script) => { return script.replace('/services/', '').replace('.js', ''); });
        },
        handler: async (ns) => {
            let server = ns.getServer();
            let script = `/services/${ns.args[1]}.js`;
            if (ns.fileExists(script)) {
                if (!ns.isRunning(script, server.hostname, 'start')) {
                    ns.run(script, 1, 'start');
                }
            }
            // let cronServer = new CronServer(ns);
            // await cronServer.init();
        }
    },
    stop: {
        args: (data, args) => {
            return data.scripts
                .filter((script) => { return script.indexOf('/services') !== -1; })
                .map((script) => { return script.replace('/services/', '').replace('.js', ''); });
        },
        handler: async (ns) => {
            let server = ns.getServer();
            let script = `/services/${ns.args[1]}.js`;
            if (ns.fileExists(script)) {
                if (ns.isRunning(script, server.hostname, 'start')) {
                    ns.run(script, 1, 'stop');
                }
            }
            // let cronServer = new CronServer(ns);
            // await cronServer.init();
        }
    },
    list: {
        args: (data, args) => {
            return [];
        },
        handler: async (ns) => {
            let server = ns.getServer();
            let services = ns.ls(server.hostname, "/services");
            services.forEach((service) => {
                let serviceName = service.replace('/services/', '').replace('.js', '');
                let isRunning = ns.isRunning(service, server.hostname, 'start');
                ns.tprint(`INFO: Service(${serviceName}) Running(${isRunning})`);
            });
            // let cronServer = new CronServer(ns);
            // await cronServer.init();
        }
    },
};
export function autocomplete(data, args) {
    if (commands[args[0]]) {
        return commands[args[0]].args(data, args);
    }
    else if (args.length === 0 || args.length === 1) {
        return Object.keys(commands);
    }
    else {
        return [];
    }
}
export async function main(ns) {
    let command = commands[ns.args[0]];
    if (command) {
        await command.handler(ns);
    }
}
