'use strict';
const glob = require('glob');
const fs = require('fs');

module.exports = grunt => {
  require('time-grunt')(grunt);
  grunt.initConfig({
    exec: {
      processTypescript: {
        command: 'npx tsc -d -p .'
      }
    }
  });
  grunt.loadNpmTasks('grunt-exec');
  grunt.registerTask('default', ['exec:processTypescript', 'compileLoaderFiles', 'exec:processTypescript']);
  grunt.registerTask('compileLoaderFiles', 'Compile a list of loader files to be downloaded', function () {
    let out= []
    let files = glob.sync('src/**/*.js');
    for (let f in files) {
      let file = files[f].substr(4);
      out.push(file);
    }
    fs.writeFileSync('loaderFiles.ts', `export const files = ${JSON.stringify(out)};`);
  });
}
