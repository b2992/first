import { NS } from "./index";
export declare function main(ns: NS): Promise<void>;
export declare function download(ns: NS, filename: string): Promise<boolean>;
